# Advent of Code
My solutions of [Advent of Code](https://adventofcode.com/), starting in 2019:

* [2019](https://bitbucket.org/OPiMedia/advent-of-code/src/master/2019/):
  only days 1 and 2 (in Python)
* [2020](https://bitbucket.org/OPiMedia/advent-of-code/src/master/2020/):
  complete days from 1 to 25 (in Python)
* [2022](https://bitbucket.org/OPiMedia/advent-of-code/src/master/2022/):
  days from 1 to 15 (in Python)



## Links
* https://www.reddit.com/r/adventofcode/
* [**HackerRank [CodinGame…] / helpers**](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  little scripts to help in solving problems of HackerRank website (or CodinGame…)
* Nicolas Viard:

    - 2019: https://github.com/Nviard/adventofcode-2019
    - 2020: https://github.com/Nviard/advent-of-code



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2019-2020, 2022 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



![Advent of Code](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/Dec/16/1409473540-4-advent-of-code-logo_avatar.png)
