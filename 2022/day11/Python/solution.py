#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 11: Monkey in the Middle
https://adventofcode.com/2022/day/11

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 12, 2022
"""

import copy
import math
import re
import sys

from typing import Dict, List


MonkeyT = Dict
MonkeysT = List[MonkeyT]


def part1(monkeys: MonkeysT) -> None:
    for _ in range(20):
        for monkey in monkeys:
            while monkey['starts']:
                monkey['nb'] += 1
                worry = monkey['starts'].pop(0)
                value = (worry if monkey['value'] == 'old'
                         else int(monkey['value']))
                if monkey['operation'] == '+':
                    worry += value
                else:
                    worry *= value
                worry //= 3
                monkeys[monkey['if_true' if worry % monkey['modulo'] == 0
                               else 'if_false']]['starts'].append(worry)

    monkeys.sort(key=lambda monkey: monkey['nb'], reverse=True)
    print(monkeys[0]['nb'] * monkeys[1]['nb'], flush=True)


def part2(monkeys: MonkeysT) -> None:
    modulos = tuple(monkey['modulo'] for monkey in monkeys)
    lcm = math.prod(modulos) // math.gcd(*modulos)

    for _ in range(10000):
        for monkey in monkeys:
            while monkey['starts']:
                monkey['nb'] += 1
                worry = monkey['starts'].pop(0)
                value = (worry if monkey['value'] == 'old'
                         else int(monkey['value']))
                if monkey['operation'] == '+':
                    worry += value
                else:
                    worry *= value
                worry %= lcm
                monkeys[monkey['if_true' if worry % monkey['modulo'] == 0
                               else 'if_false']]['starts'].append(worry)

    monkeys.sort(key=lambda monkey: monkey['nb'], reverse=True)
    print(monkeys[0]['nb'] * monkeys[1]['nb'], flush=True)


def read_data() -> MonkeysT:
    lines = tuple(map(str.rstrip, sys.stdin.readlines()))

    assert (len(lines) + 1) % 7 == 0

    seq = []
    for i in range(0, len(lines), 7):
        d: MonkeyT = {}
        match = re.match(r'Monkey (\d+):$', lines[i])
        assert match is not None
        d['n'] = int(match.group(1))

        match = re.match('  Starting items: (.+)$', lines[i + 1])
        assert match is not None
        d['starts'] = list(map(int, match.group(1).replace(',', '').split()))

        match = re.match('  Operation: new = old ([+*]) (.+)$', lines[i + 2])
        assert match is not None
        d['operation'] = match.group(1)
        d['value'] = match.group(2)

        match = re.match(r'  Test: divisible by (\d+)$', lines[i + 3])
        assert match is not None
        d['modulo'] = int(match.group(1))

        match = re.match(r'    If true: throw to monkey (\d+)$', lines[i + 4])
        assert match is not None
        d['if_true'] = int(match.group(1))

        match = re.match(r'    If false: throw to monkey (\d+)$', lines[i + 5])
        assert match is not None
        d['if_false'] = int(match.group(1))

        d['nb'] = 0

        seq.append(d)

    return seq


def main() -> None:
    monkeys = read_data()

    part1(copy.deepcopy(monkeys))
    part2(monkeys)


if __name__ == '__main__':
    main()
