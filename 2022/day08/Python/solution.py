#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 8: Treetop Tree House
https://adventofcode.com/2022/day/8

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 8, 2022
"""

import sys

from typing import Tuple


MatrixT = Tuple[Tuple[int, ...], ...]


def part1(matrix: MatrixT) -> None:
    size_height = len(matrix)
    size_width = len(matrix[0])
    visible = [[False] * size_width for _ in range(size_height)]

    for y in range(size_height):
        m = -1
        for x in range(size_width):
            if matrix[y][x] > m:
                visible[y][x] = True
            m = max(m, matrix[y][x])
        m = -1
        for x in reversed(range(size_width)):
            if matrix[y][x] > m:
                visible[y][x] = True
            m = max(m, matrix[y][x])

    for x in range(size_width):
        m = -1
        for y in range(size_height):
            if matrix[y][x] > m:
                visible[y][x] = True
            m = max(m, matrix[y][x])
        m = -1
        for y in reversed(range(size_height)):
            if matrix[y][x] > m:
                visible[y][x] = True
            m = max(m, matrix[y][x])

    print(sum(row.count(True) for row in visible))


def part2(matrix: MatrixT) -> None:
    size_height = len(matrix)
    size_width = len(matrix[0])
    visible = [[1 for _ in range(size_width)]
               for _ in range(size_height)]

    for y in range(size_height):
        for x in range(size_width):
            height = matrix[y][x]

            nb = 0
            for i in range(x + 1, size_width):
                nb += 1
                if height <= matrix[y][i]:
                    break
            visible[y][x] *= nb

            nb = 0
            for i in reversed(range(x)):
                nb += 1
                if height <= matrix[y][i]:
                    break
            visible[y][x] *= nb

            nb = 0
            for j in range(y + 1, size_height):
                nb += 1
                if height <= matrix[j][x]:
                    break
            visible[y][x] *= nb

            nb = 0
            for j in reversed(range(y)):
                nb += 1
                if height <= matrix[j][x]:
                    break
            visible[y][x] *= nb

    print(max(max(row) for row in visible))


def read_data() -> MatrixT:
    seq = []
    for line in sys.stdin:
        line = line.rstrip()
        seq.append(tuple(map(int, line)))

    return tuple(seq)


def main() -> None:
    matrix = read_data()

    part1(matrix)
    part2(matrix)


if __name__ == '__main__':
    main()
