#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 7: No Space Left On Device
https://adventofcode.com/2022/day/7

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 7, 2022
"""

import copy
import sys

from typing import Dict, List, Tuple, Union


CommandT = Dict[str, Union[str, List[str]]]


def build_tree(commands: Tuple[CommandT, ...]) -> Dict:
    sub = {'name': '/',
           'size': 0,
           'sub': {},
           'parent': None}
    tree = copy.deepcopy(sub)
    current: Dict = tree
    for command in commands:
        cmd = command['cmd']
        arg = command['arg']
        output = command['output']
        if cmd == 'cd':
            if arg == '/':
                current = tree
            elif arg == '..':
                current = current['parent']

                assert current is not None
            else:
                assert arg not in current['sub']

                current['sub'][arg] = copy.deepcopy(sub)
                current['sub'][arg]['parent'] = current
                current['sub'][arg]['name'] = arg
                current = current['sub'][arg]
        elif cmd == 'ls':
            for line in output:
                size, _ = line.split()
                if size != 'dir':
                    c = current
                    while c is not None:
                        c['size'] += int(size)
                        c = c['parent']
        else:
            assert False

    # print_tree(tree)

    return tree


def part1(commands: Tuple[CommandT, ...]) -> None:
    tree = build_tree(commands)

    sizes = []

    def walk(tree):
        if tree is not None:
            size = tree['size']
            if size <= 100000:
                sizes.append(size)
            for sub in tree['sub'].values():
                walk(sub)

    walk(tree)
    print(sum(sizes))


def part2(commands: Tuple[CommandT, ...]) -> None:
    tree = build_tree(commands)

    free = 70000000 - tree['size']
    need = 30000000 - free

    sizes = []

    def walk(tree):
        if tree is not None:
            size = tree['size']
            if size >= need:
                sizes.append(size)
            for sub in tree['sub'].values():
                walk(sub)

    walk(tree)
    sizes.sort()
    print(sizes[0])


def print_tree(tree: Dict, indent: str = '') -> None:
    if tree is not None:
        print(indent, tree['name'], ': ', tree['size'], sep='',
              file=sys.stderr)
        indent += '  '
        for sub in tree['sub'].values():
            print_tree(sub, indent)


def read_data() -> Tuple[CommandT, ...]:
    seq: List[CommandT] = []
    command = None
    for line in sys.stdin:
        line = line.rstrip()
        if line.startswith('$'):
            if command is not None:
                seq.append(command)

            parts = line.split()

            assert 2 <= len(parts) <= 3

            command = {'cmd': parts[1],
                       'arg': (parts[2] if len(parts) == 3
                               else None),
                       'output': []}
        else:
            assert command is not None

            command['output'].append(line)  # pylint: disable=E1136

    assert command is not None

    seq.append(command)

    return tuple(seq)


def main() -> None:
    commands = read_data()

    part1(commands)
    part2(commands)


if __name__ == '__main__':
    main()
