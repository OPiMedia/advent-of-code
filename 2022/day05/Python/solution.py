#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 5: Supply Stacks
https://adventofcode.com/2022/day/5

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 5, 2022
"""

import copy
import sys

from typing import List, Tuple


MoveT = Tuple[int, int, int]
StackT = List[str]


def read_moves() -> Tuple[MoveT, ...]:
    seq = []
    for line in sys.stdin:
        parts = line.split()

        assert parts[0] == 'move'
        assert parts[2] == 'from'
        assert parts[4] == 'to'

        seq.append((int(parts[1]), int(parts[3]) - 1, int(parts[5]) - 1))

    return tuple(seq)


def read_stacks() -> List[StackT]:
    seq = None
    for line in sys.stdin:
        line = line[:-1]
        if line == '':
            break

        if line.lstrip()[0] == '[':  # skip last line with indices
            items = [line[i:i + 3] for i in range(0, len(line), 4)]
            if seq is None:
                seq = [[] for _ in range(len(items))]

            for i, item in enumerate(items):
                if item[0] == '[':
                    assert seq is not None

                    seq[i].insert(0, item)

    assert seq is not None

    return seq


def main() -> None:
    data_stacks = read_stacks()
    moves = read_moves()

    # Part 1
    stacks = copy.deepcopy(data_stacks)
    for nb, a, b in moves:
        for _ in range(nb):
            item = stacks[a].pop()
            stacks[b].append(item)
    print(''.join(stack[-1][1] for stack in stacks))

    # Part 2
    stacks = copy.deepcopy(data_stacks)
    for nb, a, b in moves:
        stacks[a], items = stacks[a][:-nb], stacks[a][-nb:]
        stacks[b].extend(items)
    print(''.join(stack[-1][1] for stack in stacks))


if __name__ == '__main__':
    main()
