#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 10: Cathode-Ray Tube
https://adventofcode.com/2022/day/10

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 11, 2022
"""

import sys

from typing import Tuple


OpsT = Tuple[Tuple[str, int], ...]


def part1(ops: OpsT) -> None:
    x = 1
    i = 0
    total_strength = [0]

    def update_strenth() -> None:
        if i % 40 == 20:
            total_strength[0] += i * x

    for operation, n in ops:
        i += 1
        update_strenth()
        if operation == 'addx':
            i += 1
            update_strenth()
            x += n

    print(total_strength[0])


def part2(ops: OpsT) -> None:
    width = 40
    height = 6

    x = 1
    i = 0
    pixels = [['.' for _ in range(width)]
              for _ in range(height)]

    def update_pixels() -> None:
        pos = i - 1
        if abs(pos % width - x) <= 1:
            pixels[pos // width][pos % width] = '#'

    for operation, n in ops:
        i += 1
        update_pixels()
        if operation == 'addx':
            i += 1
            update_pixels()
            x += n

    for row in pixels:
        print(''.join(row))


def read_data() -> OpsT:
    seq = []
    for line in sys.stdin:
        pieces = line.split()

        assert 1 <= len(pieces) <= 2
        assert pieces[0] in ('addx', 'noop')

        operation = pieces[0]
        n = (int(pieces[1]) if len(pieces) == 2
             else 0)
        seq.append((operation, n))

    return tuple(seq)


def main() -> None:
    ops = read_data()

    part1(ops)
    part2(ops)


if __name__ == '__main__':
    main()
