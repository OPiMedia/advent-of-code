#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 2: Rock Paper Scissors
https://adventofcode.com/2022/day/2

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 3, 2022
"""

import sys

from typing import Tuple


def conv(c: str) -> str:
    return chr(ord(c) - ord('X') + ord('A'))


def read_data() -> Tuple[Tuple[str, str], ...]:
    seq = []

    for line in sys.stdin:
        a, b = line.split()
        seq.append((a, b))

    return tuple(seq)


def winner(a: str, b: str) -> int:
    if a == conv(b):
        return 0

    return (1 if {'X': 'C',
                  'Y': 'A',
                  'Z': 'B'}[b] == a
            else -1)


def main() -> None:
    rounds = read_data()

    # Part 1
    # A = X = Rock
    # B = Y = Paper
    # C = Z = Scissors
    n = 0
    for a, b in rounds:
        n += ord(b) - ord('X') + 1
        w = winner(a, b)
        if w == 0:
            n += 3
        elif w == 1:
            n += 6
    print(n)

    # Part 2
    # X: need lose
    # Y: need draw
    # Z: need win
    n = 0
    for a, b in rounds:
        w = {'X': -1,
             'Y': 0,
             'Z': 1}[b]
        for b in ('X', 'Y', 'Z'):
            if winner(a, b) == w:
                break

        n += ord(b) - ord('X') + 1
        if w == 0:
            n += 3
        elif w == 1:
            n += 6
    print(n)


if __name__ == '__main__':
    main()
