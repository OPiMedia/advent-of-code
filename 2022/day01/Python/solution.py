#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 1: Calorie Counting
https://adventofcode.com/2022/day/1

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 3, 2022
"""

import sys

from typing import List


def read_data() -> List[int]:
    elves = []
    n = 0
    for line in sys.stdin:
        line = line.strip()
        if line:
            n += int(line)
        else:
            elves.append(n)
            n = 0

    if n:
        elves.append(n)

    return elves


def main() -> None:
    elves = read_data()
    elves.sort(reverse=True)

    # Part 1
    print(elves[0])

    # Part 2
    print(sum(elves[:3]))


if __name__ == '__main__':
    main()
