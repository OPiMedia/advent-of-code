#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 13: Distress Signal
https://adventofcode.com/2022/day/13

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 15, 2022
"""

import functools
import json
import sys

from typing import List, Tuple, Union


def compare_int(left: int, right: int) -> int:
    return (-1 if left < right
            else (1 if left > right
                  else 0))


def compare(left: Union[int, List], right: Union[int, List]) -> int:
    if isinstance(left, int) and isinstance(right, int):
        return compare_int(left, right)

    if isinstance(left, list) and isinstance(right, list):
        i = 0
        while (i < len(left)) and (i < len(right)):
            c = compare(left[i], right[i])
            if c != 0:
                return c
            i += 1
        return compare(len(left), len(right))

    return (compare([left], right) if isinstance(left, int)
            else compare(left, [right]))


def parse(s: str) -> List:
    return json.loads(s)


def read_data() -> Tuple[Tuple[List, List], ...]:
    seq = tuple(map(str.rstrip, sys.stdin.readlines()))

    packets = []
    for i in range(0, len(seq), 3):
        packets.append((parse(seq[i]), parse(seq[i + 1])))

    return tuple(packets)


def main() -> None:
    packets = read_data()

    # Part 1
    result = 0
    for i, left_right in enumerate(packets):
        if compare(*left_right) <= 0:
            result += i + 1
    print(result)

    # Part 2
    seq = []
    seq.append(parse('[[2]]'))
    seq.append(parse('[[6]]'))
    for left, right in packets:
        seq.append(left)
        seq.append(right)

    seq.sort(key=functools.cmp_to_key(compare))

    result = 1
    for i, packet in enumerate(seq):
        if packet in ([[2]], [[6]]):
            result *= i + 1
    print(result)


if __name__ == '__main__':
    main()
