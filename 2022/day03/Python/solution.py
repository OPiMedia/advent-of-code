#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 3: Rucksack Reorganization
https://adventofcode.com/2022/day/3

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 3, 2022
"""

import sys

from typing import Tuple


def prio(c: str) -> int:
    return ord(c) + (27 - ord('A') if c <= 'Z'
                     else 1 - ord('a'))


def read_data() -> Tuple[str, ...]:
    seq = []

    for line in sys.stdin:
        line = line.rstrip()

        assert len(line) % 2 == 0

        seq.append(line)

    return tuple(seq)


def main() -> None:
    packs = read_data()

    # Part 1
    n = 0
    for pack in packs:
        middle = len(pack) // 2
        common = frozenset(pack[:middle]) & frozenset(pack[middle:])

        assert len(common) == 1

        n += sum(map(prio, common))
    print(n)

    # Part 2
    assert len(packs) % 3 == 0

    n = 0
    for i in range(0, len(packs), 3):
        common = (frozenset(packs[i]) & frozenset(packs[i + 1]) &
                  frozenset(packs[i + 2]))

        assert len(common) == 1

        n += sum(map(prio, common))
    print(n)


if __name__ == '__main__':
    main()
