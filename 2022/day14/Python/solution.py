#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 14: Regolith Reservoir
https://adventofcode.com/2022/day/14

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 16, 2022
"""

import sys

from typing import Tuple


PosT = Tuple[int, int]
PathT = Tuple[PosT, ...]
PathsT = Tuple[PathT, ...]


SOURCE = (500, 0)


def print_grid(grid) -> None:
    for y in range(10):
        print(''.join(grid.get((x, y), '.') for x in range(494, 504)),
              file=sys.stderr)
    print(file=sys.stderr, flush=True)


def part1(paths: PathsT) -> None:
    # Build grid
    grid = {}
    max_y = 0
    for path in paths:
        pos = path[0]
        for next_pos in path[1:]:
            if pos[0] == next_pos[0]:
                for y in range(min(pos[1], next_pos[1]),
                               max(pos[1], next_pos[1]) + 1):
                    grid[(pos[0], y)] = '#'
                    max_y = max(max_y, y)
            else:
                assert pos[1] == next_pos[1]

                max_y = max(max_y, pos[1])
                for x in range(min(pos[0], next_pos[0]),
                               max(pos[0], next_pos[0]) + 1):
                    grid[(x, pos[1])] = '#'

            pos = next_pos

    # Solve
    i = 0
    y = max_y
    while y <= max_y:
        i += 1
        x, y = SOURCE
        can_move = True
        while can_move and (y <= max_y):
            can_move = False
            for move_x, move_y in ((0, 1), (-1, 1), (1, 1)):
                if (x + move_x, y + move_y) not in grid:
                    x += move_x
                    y += move_y
                    can_move = True

                    break

        grid[(x, y)] = 'o'
        # print(i, file=sys.stderr)
        # print_grid(grid)

    print(i - 1)


def part2(paths: PathsT) -> None:
    # Build grid
    grid = {}
    max_y = 0
    for path in paths:
        pos = path[0]
        for next_pos in path[1:]:
            if pos[0] == next_pos[0]:
                for y in range(min(pos[1], next_pos[1]),
                               max(pos[1], next_pos[1]) + 1):
                    grid[(pos[0], y)] = '#'
                    max_y = max(max_y, y)
            else:
                assert pos[1] == next_pos[1]

                max_y = max(max_y, pos[1])
                for x in range(min(pos[0], next_pos[0]),
                               max(pos[0], next_pos[0]) + 1):
                    grid[(x, pos[1])] = '#'

            pos = next_pos

    # Solve
    i = 0
    while True:
        i += 1
        x, y = SOURCE
        can_move = True
        while can_move:
            can_move = False
            for move_x, move_y in ((0, 1), (-1, 1), (1, 1)):
                if ((y + move_y <= max_y + 1) and
                        (x + move_x, y + move_y) not in grid):
                    x += move_x
                    y += move_y
                    can_move = True

                    break

        if (x, y) in grid:
            break

        grid[(x, y)] = 'o'
        # print(i, file=sys.stderr)
        # print_grid(grid)

    print(i - 1)


def read_data() -> PathsT:
    def parse_pos(s: str) -> PosT:
        pos = tuple(map(int, s.split(',')))

        assert len(pos) == 2

        return pos  # type: ignore

    seq = []
    for line in sys.stdin:
        seq.append(tuple(map(parse_pos, line.rstrip().split(' -> '))))

    return tuple(seq)


def main() -> None:
    paths = read_data()

    part1(paths)
    part2(paths)


if __name__ == '__main__':
    main()
