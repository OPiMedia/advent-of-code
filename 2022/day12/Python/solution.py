#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 12: Hill Climbing Algorithm
https://adventofcode.com/2022/day/12

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 14, 2022
"""

import collections
import sys

from typing import Deque, Tuple


MatrixT = Tuple[str, ...]
PosT = Tuple[int, int]


def get_level(c: str) -> int:
    return ord(c) - ord('a')


def solve(start: PosT, end: PosT, matrix: MatrixT) -> int:  # noqa  # pylint: disable=too-many-locals
    height = len(matrix)
    width = len(matrix[0])
    min_length = sys.maxsize

    lengths = {}
    for y in range(height):
        for x in range(width):
            lengths[(x, y)] = min_length

    queue: Deque[Tuple[PosT, int]] = collections.deque()
    queue.append((start, 0))
    while queue:
        pos, length = queue.popleft()
        if pos == end:
            min_length = min(min_length, length)
        else:
            length += 1
            x, y = pos
            for move_x, move_y in ((1, 0), (-1, 0), (0, 1), (0, -1)):
                next_x = x + move_x
                next_y = y + move_y
                if ((0 <= next_x < width) and (0 <= next_y < height) and
                        (lengths[(next_x, next_y)] > length) and
                        (get_level(matrix[next_y][next_x]) <=
                         get_level(matrix[y][x]) + 1)):
                    lengths[(next_x, next_y)] = length
                    queue.append(((next_x, next_y), length))

    return min_length


def read_data() -> Tuple[PosT, PosT, MatrixT]:
    seq = list(map(str.rstrip, sys.stdin.readlines()))

    for start_y, row in enumerate(seq):
        start_x = row.find('S')
        if start_x != -1:
            seq[start_y] = seq[start_y].replace('S', 'a')

            break

    for end_y, row in enumerate(seq):
        end_x = row.find('E')
        if end_x != -1:
            seq[end_y] = seq[end_y].replace('E', 'z')

            break

    return (start_x, start_y), (end_x, end_y), tuple(seq)  # noqa  # pylint: disable=undefined-loop-variable


def main() -> None:
    start, end, matrix = read_data()

    # Part 1
    print(solve(start, end, matrix))

    # Part 2
    height = len(matrix)
    width = len(matrix[0])
    min_length = sys.maxsize

    for y in range(height):
        for x in range(width):
            if matrix[y][x] == 'a':
                min_length = min(min_length, solve((x, y), end, matrix))

    print(min_length)


if __name__ == '__main__':
    main()
