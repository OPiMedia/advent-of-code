#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 6: Tuning Trouble
https://adventofcode.com/2022/day/6

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 6, 2022
"""

import sys


def read_data() -> str:
    for line in sys.stdin:
        return line.rstrip()

    assert False


def solve(s: str, nb: int) -> int:
    for i in range(len(s)):
        if len(set(s[i - nb:i])) == nb:
            return i

    assert False


def main() -> None:
    s = read_data()

    # Part 1
    print(solve(s, 4))

    # Part 2
    print(solve(s, 14))


if __name__ == '__main__':
    main()
