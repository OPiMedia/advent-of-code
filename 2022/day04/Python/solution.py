#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 4: Camp Cleanup
https://adventofcode.com/2022/day/4

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 4, 2022
"""

import sys

from typing import Tuple


PairT = Tuple[int, int]


def read_data() -> Tuple[Tuple[PairT, PairT], ...]:
    def pair(s: str) -> PairT:
        p = tuple(map(int, s.split('-')))

        assert len(p) == 2

        return p[0], p[1]  # to avoid false type error

    seq = []
    for line in sys.stdin:
        a, b = map(pair, line.split(','))

        assert a[0] <= a[1]
        assert b[0] <= b[1]

        seq.append((a, b))

    return tuple(seq)


def main() -> None:
    pairs = read_data()

    # Part 1
    n = 0
    for a, b in pairs:
        if (a[0] <= b[0] <= b[1] <= a[1]) or (b[0] <= a[0] <= a[1] <= b[1]):
            n += 1
    print(n)

    # Part 2
    n = 0
    for a, b in pairs:
        if (a[0] <= b[0] <= a[1]) or (b[0] <= a[0] <= b[1]):
            n += 1
    print(n)


if __name__ == '__main__':
    main()
