#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 9: Rope Bridge
https://adventofcode.com/2022/day/9

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 9, 2022
"""

import sys

from typing import Tuple


MovesT = Tuple[Tuple[str, int]]


SHIFTS = {'U': (0, 1),
          'D': (0, -1),
          'R': (1, 0),
          'L': (-1, 0)}


def move_tail(head: Tuple[int, int], tail: Tuple[int, int]) -> Tuple[int, int]:
    x, y = head
    p, q = tail
    if (abs(p - x) > 1) or (abs(q - y) > 1):  # need to move queue
        if y != q:  # different line
            if q < y:
                q += 1
            else:
                q -= 1
        if x != p:  # different column
            if p < x:
                p += 1
            else:
                p -= 1

    return p, q


def part1(moves: MovesT) -> None:
    x, y = 0, 0  # head
    p, q = 0, 0  # queue
    visited = set()
    for direction, nb in moves:
        shift = SHIFTS[direction]
        for _ in range(nb):
            x += shift[0]
            y += shift[1]
            p, q = move_tail((x, y), (p, q))
            visited.add((p, q))

    print(len(visited))


def part2(moves: MovesT) -> None:
    visited = set()
    rope = [(0, 0) for _ in range(10)]
    for direction, nb in moves:
        shift = SHIFTS[direction]
        for _ in range(nb):
            rope[0] = (rope[0][0] + shift[0], rope[0][1] + shift[1])
            for i in range(1, len(rope)):
                rope[i] = move_tail(rope[i - 1], rope[i])

            visited.add(tuple(rope[-1]))

    print(len(visited))


def read_data() -> MovesT:
    seq = []
    for line in sys.stdin:
        direction, nb = line.rstrip().split()
        seq.append((direction, int(nb)))

    return tuple(seq)


def main() -> None:
    moves = read_data()

    part1(moves)
    part2(moves)


if __name__ == '__main__':
    main()
