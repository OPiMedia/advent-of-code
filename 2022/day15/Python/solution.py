#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2022 - Day 15: Beacon Exclusion Zone
https://adventofcode.com/2022/day/15

:license: GPLv3 --- Copyright (C) 2022 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 17, 2022
"""

import re
import sys

from typing import Set, Tuple


PosT = Tuple[int, int]
PositionsT = Tuple[Tuple[PosT, PosT], ...]


def distance(a, b: PosT) -> int:
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


def part1(positions: PositionsT) -> None:
    row = (10 if len(positions) == 14
           else 2_000_000)

    # grid = {}
    beacon_xs = set()
    for pos, beacon in positions:
        # assert pos not in grid

        # grid[pos] = beacon
        if beacon[1] == row:
            beacon_xs.add(beacon[0])

    # print(beacon_xs, file=sys.stderr)
    # if row == 10:
    #     print_grid(positions)

    impossibles: Set[int] = set()
    for pos, beacon in positions:
        d = distance(pos, beacon) - abs(row - pos[1])
        impossibles.update(x for x in range(pos[0] - d, pos[0] + d + 1))

    print(len(impossibles - beacon_xs), flush=True)


def part2(positions: PositionsT) -> None:
    """
    Inspired by
    https://github.com/noah-clements/AoC2022/blob/master/day15/day15.py#L59
    """
    max_size = (20 if len(positions) == 14
                else 4_000_000)
    distances = tuple((pos, distance(pos, beacon))
                      for pos, beacon in positions)
    points = set()
    for pos, d in distances:
        for sign in (1, -1):
            for i in range(d + 1):
                for move_x, move_y in ((d + 1 - i, i),
                                       (-i, d + 1 - i)):
                    x = pos[0] + sign * move_x
                    y = pos[1] + sign * move_y
                    if ((0 <= x <= max_size) and (0 <= y <= max_size)
                            and ((x, y) not in points)
                            and all(distance((x, y), other) > other_d
                                    for other, other_d in distances)):
                        print(x * 4_000_000 + y)

                        return

                    points.add((x, y))


def print_grid(positions: PositionsT) -> None:
    x0 = -4
    y0 = -2
    width = 31
    height = 25
    plan = [['.'] * width for _ in range(height)]

    for pos, beacon in positions:
        plan[pos[1] - y0][pos[0] - x0] = 'S'
        plan[beacon[1] - y0][beacon[0] - x0] = 'B'

    for y in range(y0, y0 + height):
        for x in range(x0, x0 + width):
            is_possible = True
            for pos, beacon in positions:
                # if pos != (8, 7):
                #     continue

                if distance(pos, (x, y)) <= distance(pos, beacon):
                    is_possible = False

                    break

            if not is_possible and (plan[y - y0][x - x0] == '.'):
                plan[y - y0][x - x0] = '#'

    print('  ', ''.join(str(x % 10) for x in range(x0, x0 + width)),
          file=sys.stderr)
    for y in range(y0, y0 + height):
        print(f'{y:2}', ''.join(plan[y - y0]), file=sys.stderr)


def read_data() -> PositionsT:
    seq = []
    for line in sys.stdin:
        match = re.search(r'Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)$', line.rstrip())  # noqa

        assert match, line

        pos = (int(match.group(1)), int(match.group(2)))
        beacon = (int(match.group(3)), int(match.group(4)))
        seq.append((pos, beacon))

    return tuple(seq)


def main() -> None:
    positions = read_data()

    part1(positions)
    part2(positions)  # takes ~ 3 minutes


if __name__ == '__main__':
    main()
