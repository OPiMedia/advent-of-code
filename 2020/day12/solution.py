#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 12: Rain Risk
https://adventofcode.com/2020/day/12

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 12, 2020
"""

import sys


def main() -> None:
    moves = []
    for line in sys.stdin:
        line = line.rstrip()
        action = line[0]
        value = int(line[1:])
        moves.append((action, value))

    # Part 1
    direction = [1, 0]  # east
    pos = [0, 0]

    for action, value in moves:
        if action == 'N':    # north
            pos[1] += value
        elif action == 'S':  # south
            pos[1] -= value
        elif action == 'E':  # east
            pos[0] += value
        elif action == 'W':  # west
            pos[0] -= value
        elif action in 'LR':  # left or right
            if action == 'L':
                value = 360 - value

            assert value in (90, 180, 270)

            value //= 90
            for _ in range(value):
                direction = [direction[1], -direction[0]]
        elif action == 'F':  # forward
            pos[0] += direction[0] * value
            pos[1] += direction[1] * value
        else:
            assert False

        # print(action, value, pos, direction, file=sys.stderr)

    # print(file=sys.stderr)
    print(abs(pos[0]) + abs(pos[1]))

    # Part 2
    waypoint = [10, 1]
    pos = [0, 0]

    for action, value in moves:
        if action == 'N':    # north
            waypoint[1] += value
        elif action == 'S':  # south
            waypoint[1] -= value
        elif action == 'E':  # east
            waypoint[0] += value
        elif action == 'W':  # west
            waypoint[0] -= value
        elif action in 'LR':  # left or right
            if action == 'L':
                value = 360 - value

            assert value in (90, 180, 270)

            value //= 90
            for _ in range(value):
                waypoint = [waypoint[1], -waypoint[0]]
        elif action == 'F':  # forward
            pos[0] += waypoint[0] * value
            pos[1] += waypoint[1] * value
        else:
            assert False

        # print(action, value, pos, direction, waypoint, file=sys.stderr)

    print(abs(pos[0]) + abs(pos[1]))


if __name__ == '__main__':
    main()
