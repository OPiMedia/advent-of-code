#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 3: Toboggan Trajectory
https://adventofcode.com/2020/day/3

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 3, 2020
"""

import functools
import operator
import sys


def main() -> None:
    offsets = ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))

    nbs = [0] * len(offsets)
    xs = [0] * len(offsets)

    for y, line in enumerate(sys.stdin):
        line = line.rstrip()

        for i, offset_xy in enumerate(offsets):
            offset_x, offset_y = offset_xy
            if y % offset_y == 0:
                if line[xs[i]] == '#':
                    nbs[i] += 1
                xs[i] = (xs[i] + offset_x) % len(line)

    # Part 1
    print(nbs[1])

    # Part 2
    print(nbs, file=sys.stderr)
    print(functools.reduce(operator.mul, nbs))


if __name__ == '__main__':
    main()
