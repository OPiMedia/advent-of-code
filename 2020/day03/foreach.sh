#!/bin/bash

#
# Usage: foreach.sh [options] [COMMON...]
#
# For each COMMON run "run.sh COMMON".
# At the end summarize results.
#
# If no parameter is given
# then set COMMON... with all COMMON found as "data/input/inputCOMMON.txt" files.
#
# Standard and error outputs from each "run.sh"
# are redirected to "result/result/foreach_logCOMMON.txt".
#
# In the summary, print content of result and output files when are different.
#
# If file "config.sh" exists in the current working directory
# then it is loaded to override configuration variables.
#
# If file "run.sh" exists in the current working directory
# then run it by "./run.sh",
# else run "run.sh" reachable in the PATH.
#
# Options:
#   -e   pass -e option to each "run.sh" execution
#
#   -l   only print list of common parts and corresponding input/output files,
#          don't run anything
#   -L   same than -l but print URL of files
#
#   -m   pass -m option to each "run.sh" execution
#   -M   pass -m option to the first "run.sh" execution
#
#   -s   stop after the first "run.sh" execution that fails
#   -t   pure text, disable ANSI colors
#
#   -v   launch difference viewer (like meld, kdiff3...) specified by DIFF_VIEW
#          when found difference between result and output files
#   -V   like -v, but only once, on the first difference found
#
#   -z   pass -z option to each "run.sh" execution
#   -Z   pass -z option to the first "run.sh" execution
#
#   --   mark the end of foreach.sh options, remain is considered as COMMON
#
#   --help   display this help message and quit
#
# Exit status: number of failed executions
#   (more precisely 0 if all executions was OK,
#    else (n-1)%255 + 1 where n = number of failed executions)
#
#
# Requirements:
# * run.sh
# * cat, head, mkdir, tee
# * meld of -v and -V options
#
#
# Latest version of "HackerRank [CodinGame...] / helpers",
# config files for several programming languages,
# and more explanation on Bitbucket:
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/
#
# GPLv3 --- Copyright (C) 2020 Olivier Pirson
# http://www.opimedia.be/
#
# * December 6, 2020: Replaced default value 1 of *_HEAD by 3.
# * December 5, 2020:
#   - Corrected common identification when wildcard finishes input pattern.
#   - Replaced special characters %s in patterns by the single *.
#   - Replaced wrong postfix name by common.
# * September 20, 2020:
#   - Added -s option.
#   - Replaced magenta color of title by yellow.
# * September 16, 2020: Added -z and -Z options.
# * September 8, 2020:
#   - Split data/ directory in data/input/input and data/correct.
# * August 24, 2020:
#   - Added mark to extract list of configuration variables in README.
# * August 19, 2020.
# * Started August 9, 2020.
##

#
# Configuration variables
#
# <<< begin of configuration variables
INPUT_PATTERN='data/input/input*.txt'    # pattern of input files
OUTPUT_PATTERN='data/correct/output*.txt'  # pattern of corresponding correct output files

RUN='run.sh'  # script executed for each COMMON
declare -ag RUN_FLAGS=()

if [ -f "./$RUN" ]; then
    RUN="./$RUN"
fi

RESULT_PATTERN='result/result*.txt'   # pattern of result files
RESULT_LOG_PATTERN='result/foreach_log*.txt'  # pattern of result log files

DIFF_VIEW='meld'  # differences viewer, enabled by -v or -V options
declare -ag DIFF_VIEW_FLAGS=('-n')

declare -ig DIFF_VIEW_PRINT=0  # if 0 then print command to launch DIFF_VIEW when different

# Set '' to print all content, or 0 to no print
RESULT_HEAD=3  # max number of lines to print, for result file
OUTPUT_HEAD=3  # max number of lines to print, for output file

EMPTY_COMMON='__empty_common__'  # special value to deal with empty key '' in associative table

declare -ig DIFF_VIEW_RUN=1       # if 0 then run difference viewer when different files
declare -ig DIFF_VIEW_RUN_ONCE=1  # if 0 then run difference viewer only on first different files

declare -ig ONLY_PRINT_LIST=1      # if 0 then only print list, don't run anything
declare -ig ONLY_PRINT_LIST_URL=1  # if 0 then print URLs instead filenames when print list

declare -ig STOP_AFTER_RUN_FAIL=1  # if 0 then stop after the first "run.sh" execution that fails


E_NORMAL='\e[0m'

E_GREEN='\e[92m'
E_YELLOW='\e[93m'

E_BG_BLUE='\e[44m'
E_BG_RED='\e[101m'
# end of configuration variables >>>


CONFIG='config.sh'

if [ -f "$CONFIG" ]; then
    # shellcheck disable=SC1090
    source "$CONFIG"
fi



#
# Functions
#

# Print content of $1 file.
# If $2 is specified
# then only print $2 first lines.
#      In this case, if there is more lines then print ... and number of lines.
cat_head() {
    local -r filename=$1
    declare -ir n=$2

    if [ -f "$filename" ]; then
        if [ -s "$filename" ]; then
            if [ -z "$2" ]; then
                printf '  '"$E_BG_BLUE"'<%s>'"$E_NORMAL"'\n' "$(url "$filename")"
                cat "$filename"
            else
                if [ "$n" -ne 0 ]; then
                    printf '  '"$E_BG_BLUE"'<%s>'"$E_NORMAL"'\n' "$(url "$filename")"

                    declare -i length

                    length=$(wc -l < "$filename")
                    head -n "$n" < "$filename"
                    if [ "$length" -gt "$n" ]; then
                        echo "  [... $n/$length]"
                    fi
                fi
            fi
        else
            if [ "$n" -ne 0 ]; then
                printf '  '"$E_BG_BLUE"'[empty file "%s"]'"$E_NORMAL"'\n' "$filename"
            fi
        fi
    else
        printf '  '
        print_colored '[File "'"$filename"'" doesn'"'"'t exist!]'
        echo

        return 1
    fi
}


# Check if all patterns contain the right number of wildcard *.
# If not print the help message and exit 1.
check_patterns() {
    local pattern
    local removed

    for name_pattern in INPUT_PATTERN OUTPUT_PATTERN RESULT_PATTERN RESULT_LOG_PATTERN; do
        pattern="${!name_pattern}"
        removed=$(wildcard_substitute "$pattern")
        if [ "$name_pattern" == 'INPUT_PATTERN' ]; then
            if [ $((${#pattern} - ${#removed})) -ne 1 ]; then
                echo "!$name_pattern must contain exactly one wildcard *"
                print_help

                exit 1
            fi
        else
            if [ $((${#pattern} - ${#removed})) -lt 1 ]; then
                echo "!$name_pattern must contain at least one wildcard *"
                print_help

                exit 1
            fi
        fi
    done
}


# Print text $1.
# If $2 then print it on red background.
print_colored() {
    local -r text=$1
    local -r error=$2

    # shellcheck disable=SC2059
    if [[ "$error" -eq 0 ]]; then printf "$E_BG_RED"; fi

    printf '%s' "$text"

    # shellcheck disable=SC2059
    if [[ "$error" -eq 0 ]]; then printf "$E_NORMAL"; fi
}


# Print text $1 on yellow background.
print_colored_title() {
    printf "$E_YELLOW"'==================== %s ===================='"$E_NORMAL"'\n' "$1"
}


# Print help message (read from the script itself).
print_help() {
    local line
    local -i skip=0

    while read -r line; do
        if [[ "$line" == '# Usage:'* ]]; then skip=1; fi
        if [ "$skip" -eq 0 ]; then continue; fi
        if [[ "$line" == '##'* ]]; then break; fi
        echo "${line:2}"
    done <<< "$(cat "${BASH_SOURCE[0]}")";
}


# Print URL of the file $1.
url() {
    echo "file://$PWD/$1"
}


# Return $1 with each * character substituted by $2.
wildcard_substitute() {
    echo "${1//\*/$2}"
}



# Print list of COMMON_PARTS and corresponding files (or URLs if ONLY_PRINT_LIST_URL).
main__print_list() {
    local text='Corresponding input file:'
    declare -i length=${#text}
    declare -i max_length
    local common
    local input
    local tmp

    max_length="${#text}"
    for common in "${COMMON_PARTS[@]}"; do
        input=$(wildcard_substitute "$INPUT_PATTERN" "$common")
        if [ "$ONLY_PRINT_LIST_URL" -eq 0 ]; then
            tmp="<$(url "$input")>"
        else
            tmp='"'"$input"'"'
        fi
        if [ "$max_length" -lt "${#tmp}" ]; then
            max_length=${#tmp}
        fi
    done

    printf '%-16s\t%4s' 'Common:' "$text"
    printf ' %.0s' $(seq "$length" "$max_length")
    printf '  \tCorresponding correct output file:\n'

    local -i code
    local output

    for common in "${COMMON_PARTS[@]}"; do
        input=$(wildcard_substitute "$INPUT_PATTERN" "$common")
        printf '"%-16s\t' "$common"'"'
        [ ! -f "$input" ]
        code=$?
        if [ "$ONLY_PRINT_LIST_URL" -eq 0 ]; then
            text="<$(url "$input")>"
        else
            text='"'"$input"'"'
        fi
        print_colored "$text" "$code"

        output=$(wildcard_substitute "$OUTPUT_PATTERN" "$common")
        length=${#text}
        printf ' %.0s' $(seq "$length" "$max_length")
        printf '  \t'
        [ ! -f "$output" ]
        code=$?
        if [ "$ONLY_PRINT_LIST_URL" -eq 0 ]; then
            text="<$(url "$output")>"
        else
            text='"'"$output"'"'
        fi
        print_colored "$text" "$code"
        echo
    done
}


# Print summary, from COMMON_PARTS and CODES.
main__print_summary() {
    declare -Ar steps=( [50]='no run' [100]='make failed' [101]='input failed' [102]='run failed' [103]='compare failed' [104]='result failed' [105]='output failed' )
    declare -i ok=0
    declare -i nb=0
    local common
    local key
    declare -i code
    local common_quotted
    local text
    local result_log
    local result
    local output

    print_colored_title 'SUMMARY'
    for common in "${COMMON_PARTS[@]}"; do
        nb+=1

        if [ -z "$common" ]; then
            key="$EMPTY_COMMON"
        else
            key="$common"
        fi

        code=${CODES["$key"]}

        common_quotted='"'"$common"'"'
        if [ "$code" -eq 0 ]; then
            ok+=1
            printf ''"$E_GREEN"'= %-10s\tok'"$E_NORMAL"'\n' "$common_quotted"
        else
            text=$(printf '! %-10s\t%-3s\t%s' "$common_quotted" "$code" "${steps["$code"]}")
            print_colored "$text"
            echo

            if [ "$code" -ne 50 ]; then  # "run.sh" was executed and failed
                result_log=$(wildcard_substitute "$RESULT_LOG_PATTERN" "$common")
                result=$(wildcard_substitute "$RESULT_PATTERN" "$common")
                output=$(wildcard_substitute "$OUTPUT_PATTERN" "$common")

                printf '<%s>\t<%s>\t<%s>\n' "$(url "$result_log")" "$(url "$result")" "$(url "$output")"

                if [ "$DIFF_VIEW_PRINT" -eq 0 ]; then
                    echo "$DIFF_VIEW ${DIFF_VIEW_FLAGS[*]} $result $output &"
                fi

                cat_head "$result" "$RESULT_HEAD"
                cat_head "$output" "$OUTPUT_HEAD"
            fi
        fi
    done

    echo
    echo "OK $ok / $nb"

    code=$(( nb - ok ))

    if [ $code -ne 0 ]; then
        code=$(( (code - 1) % 255 + 1 ))
    fi

    return $code
}


# For each common from COMMON_PARTS,
# run run.sh on common,
# write both to standard output and log file,
# and set error code in CODES
main__run_all() {
    local common
    local result_log
    local code
    local key
    local output
    local result
    local stopped=1

    for common in "${COMMON_PARTS[@]}"; do
        if [ -z "$common" ]; then
            key="$EMPTY_COMMON"
        else
            key="$common"
        fi

        if [ "$stopped" -ne 0 ]; then
            print_colored_title '"'"$common"'"'

            result_log=$(wildcard_substitute "$RESULT_LOG_PATTERN" "$common")

            mkdir -p "$(dirname "$result_log")"

            # shellcheck disable=SC2086
            "$RUN" ${RUN_FLAGS[*]} -- "$common" 2>&1 | tee "$result_log"

            code="${PIPESTATUS[0]}"
            CODES["$key"]="$code"

            echo

            if [[ "$code" -eq 103 && "$DIFF_VIEW_RUN" -eq 0 ]]; then
                result=$(wildcard_substitute "$RESULT_PATTERN" "$common")
                output=$(wildcard_substitute "$OUTPUT_PATTERN" "$common")

                # shellcheck disable=SC2086
                "$DIFF_VIEW" ${DIFF_VIEW_FLAGS[*]} "$result" "$output" &

                if [ "$DIFF_VIEW_RUN_ONCE" -eq 0 ]; then
                    DIFF_VIEW_RUN_ONCE=1
                    DIFF_VIEW_RUN=1
                fi
            fi

            if [[ "$STOP_AFTER_RUN_FAIL" -eq 0 && "$code" -ne 0 ]]; then
                stopped=0
                print_colored 'STOP' 0
                echo
            fi

            RUN_FLAGS=("${RUN_FLAGS_NEXT[@]}")
        else
            CODES["$key"]=50
        fi
    done
}


# Set options from $@
# and set remains parameters in COMMON_PARTS.
main__set_options() {
    declare -i found=0

    while [ "$found" -eq 0 ]; do
        case "$1" in
            --help)
                print_help

                exit 0
                ;;
            -e)  # pass -e to run.sh
                RUN_FLAGS+=("$1")
                RUN_FLAGS_NEXT+=("$1")
                ;;
            -l)  # only print list of common parts
                ONLY_PRINT_LIST=0
                ;;
            -L)  # like -l but with URL
                ONLY_PRINT_LIST=0
                ONLY_PRINT_LIST_URL=0
                ;;
            -m)  # pass -m to run.sh
                RUN_FLAGS+=("$1")
                RUN_FLAGS_NEXT+=("$1")
                ;;
            -M)  # pass -m to first execution of run.sh
                RUN_FLAGS+=('-m')
                ;;
            -s)  # stop after first "run.sh" that fails
                STOP_AFTER_RUN_FAIL=0
                ;;
            -t)  # disable ANSI colors
                E_NORMAL=''
                E_GREEN=''
                E_YELLOW=''
                E_BG_BLUE=''
                E_BG_RED=''
                RUN_FLAGS+=('-t')
                RUN_FLAGS_NEXT+=('-t')
                ;;
            -v)  # launch difference viewer
                DIFF_VIEW_RUN=0
                DIFF_VIEW_RUN_ONCE=1
                ;;
            -V)  # like -v but only once
                DIFF_VIEW_RUN=0
                DIFF_VIEW_RUN_ONCE=0
                ;;
            -z)  # pass -z to run.sh
                RUN_FLAGS+=("$1")
                RUN_FLAGS_NEXT+=("$1")
                ;;
            -Z)  # pass -z to first execution of run.sh
                RUN_FLAGS+=('-z')
                ;;
            --)  # end of options
                shift

                break
                ;;
            *)
                found=1
                ;;
        esac
        if [ "$found" -eq 0 ]; then
            shift
        fi
    done

    COMMON_PARTS=("$@")
}


# Set COMMON_PARTS from files corresponding to input pattern.
main__set_common_parts() {
    local -r begin="${INPUT_PATTERN/\**/}"  # part before the wildcard
    declare -ir begin_length=${#begin}
    declare -ir end_length=$((${#INPUT_PATTERN} - 1 - begin_length))
    declare -i common_length
    local common
    local input

    for input in $(compgen -G "$INPUT_PATTERN" | sort); do
        if [ -f "$input" ]; then  # skip directory
            common_length=$((${#input} - begin_length - end_length))
            common="${input:$begin_length:$common_length}"
            COMMON_PARTS+=("$common")
        fi
    done
}



#
# Main
#

# Set options and COMMON_PARTS
declare -ag COMMON_PARTS

RUN_FLAGS_NEXT=("${RUN_FLAGS[*]}")

check_patterns

main__set_options "$@"

if [ "${#COMMON_PARTS[@]}" -eq 0 ]; then
    main__set_common_parts
fi


# If -l option then print list of COMMON_PARTS and exit
if [ "$ONLY_PRINT_LIST" -eq 0 ]; then
    main__print_list

    exit 0
fi



# Run and print summary
declare -Ag CODES

main__run_all
main__print_summary
