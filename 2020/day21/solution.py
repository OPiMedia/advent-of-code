#!/usr/bin/env pypy3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 21: Allergen Assessment
https://adventofcode.com/2020/day/21

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 21, 2020
"""

import re
import sys


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def logn(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
         flush: bool = True) -> None:
    for param in params:
        log(param, sep=sep, end=end, file=file, flush=flush)


def main() -> None:
    # Read data
    regex = re.compile(r'(.+) \(contains (.+)\)')
    foods = []
    all_ingredients = set()
    all_allergens = set()
    for line in sys.stdin:
        match = regex.fullmatch(line.rstrip())

        assert match

        ingredients = tuple(match.group(1).split())
        all_ingredients |= set(ingredients)

        allergens = tuple(match.group(2).split(', '))
        all_allergens |= set(allergens)

        food = (ingredients, allergens)
        foods.append(food)

    # Part 1
    foods.sort(key=lambda food: (len(food[1]), len(food[0])))

    possibles = {allergen: set(all_ingredients) for allergen in all_allergens}
    for ingredients, allergens in foods:
        for allergen in allergens:
            possibles[allergen] &= set(ingredients)

            assert possibles[allergen]

    part2 = dict()
    modified = True
    while modified:
        modified = False
        for allergen, set_ingredients in tuple(possibles.items()):
            if len(set_ingredients) == 1:
                del possibles[allergen]
                ingredient = tuple(set_ingredients)[0]
                part2[allergen] = ingredient
                all_ingredients.remove(ingredient)
                for allergen2 in possibles:
                    possibles[allergen2].discard(ingredient)
                modified = True

    nb = 0
    for ingredients, _ in foods:
        for ingredient in ingredients:
            if ingredient in all_ingredients:
                nb += 1

    print(nb)

    # Part 2
    print(','.join(ingredient for _, ingredient in sorted(part2.items())))


if __name__ == '__main__':
    main()
