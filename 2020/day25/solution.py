#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 25: Combo Breaker
https://adventofcode.com/2020/day/25

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 25, 2020
"""

import sys


def encrypt(subject: int, size: int) -> int:
    n = 1
    for _ in range(size):
        n = (n * subject) % 20201227

    return n


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def loop_size(key: int) -> int:
    n = 1
    i = 0

    while n != key:
        n = (n * 7) % 20201227
        i += 1

    return i


def main() -> None:
    card_public_key = int(input())
    door_public_key = int(input())

    card_loop_size = loop_size(card_public_key)
    door_loop_size = loop_size(door_public_key)

    log(card_public_key, card_loop_size)
    log(door_public_key, door_loop_size)

    assert encrypt(7, card_loop_size) == card_public_key
    assert encrypt(7, door_loop_size) == door_public_key

    door_encrypted = encrypt(door_public_key, card_loop_size)
    card_encrypted = encrypt(card_public_key, door_loop_size)

    log(door_encrypted)
    log(card_encrypted)

    print(door_encrypted)

    assert card_encrypted == door_encrypted


if __name__ == '__main__':
    main()
