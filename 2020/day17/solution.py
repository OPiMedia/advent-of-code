#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 17: Conway Cube
https://adventofcode.com/2020/day/17

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 17, 2020
"""

import sys

from typing import List

import numpy as np


def part1(data: List[str]) -> int:
    def nb_active(space) -> int:
        nb = 0
        for plane in space:
            for row in plane:
                for cell in row:
                    if cell:
                        nb += 1

        return nb

    def neightbour_nb_active(space, x: int, y: int, z: int) -> int:
        nb = 0
        for x_diff in (-1, 0, 1):
            for y_diff in (-1, 0, 1):
                for z_diff in (-1, 0, 1):
                    if (x_diff != 0) or (y_diff != 0) or (z_diff != 0):
                        if space[x + x_diff, y + y_diff, z + z_diff]:
                            nb += 1

        return nb

    nb_iter = 6
    first = nb_iter + 1

    x_zone = [first, first]
    y_zone = [first, first]
    z_zone = [first, first]

    def update_zone(x: int, y: int, z: int) -> None:
        x_zone[0] = min(x_zone[0], x)
        x_zone[1] = max(x_zone[1], x)
        y_zone[0] = min(y_zone[0], y)
        y_zone[1] = max(y_zone[1], y)
        z_zone[0] = min(z_zone[0], z)
        z_zone[1] = max(z_zone[1], z)

    # Init with data
    size = 0
    for y, line in enumerate(data):
        if size == 0:
            size = len(line) + (nb_iter + 1) * 2
            space = np.zeros([size, size, size], bool)
            new_space = np.zeros([size, size, size], bool)
        for x, cell in enumerate(line):
            x_shifted = x_zone[0] + x
            y_shifted = y_zone[0] + y
            z_shifted = z_zone[0] + 0
            if cell == '#':
                space[x_shifted, y_shifted, z_shifted] = True
                update_zone(x_shifted, y_shifted, z_shifted)

    # Iterate
    for _ in range(6):
        x_first = x_zone[0] - 1
        x_last = x_zone[1] + 1
        y_first = y_zone[0] - 1
        y_last = y_zone[1] + 1
        z_first = z_zone[0] - 1
        z_last = z_zone[1] + 1
        for x in range(x_first, x_last + 1):
            for y in range(y_first, y_last + 1):
                for z in range(z_first, z_last + 1):
                    nb = neightbour_nb_active(space, x, y, z)
                    new_space[x, y, z] = ((nb == 3) or
                                          (space[x, y, z] and (nb == 2)))
                    if new_space[x, y, z]:
                        update_zone(x, y, z)

        space = new_space
        new_space = np.zeros([size, size, size], bool)

    return nb_active(space)


def part2(data: List[str]) -> int:
    def nb_active(space) -> int:
        nb = 0
        for hyper in space:
            for plane in hyper:
                for row in plane:
                    for cell in row:
                        if cell:
                            nb += 1

        return nb

    def neightbour_nb_active(space, x: int, y: int, z: int, w: int) -> int:
        nb = 0
        for x_diff in (-1, 0, 1):
            for y_diff in (-1, 0, 1):
                for z_diff in (-1, 0, 1):
                    for w_diff in (-1, 0, 1):
                        if ((x_diff != 0) or (y_diff != 0) or (z_diff != 0)
                                or (w_diff != 0)):
                            if space[x + x_diff, y + y_diff, z + z_diff,
                                     w + w_diff]:
                                nb += 1

        return nb

    nb_iter = 6
    first = nb_iter + 1

    x_zone = [first, first]
    y_zone = [first, first]
    z_zone = [first, first]
    w_zone = [first, first]

    def update_zone(x: int, y: int, z: int, w: int) -> None:
        x_zone[0] = min(x_zone[0], x)
        x_zone[1] = max(x_zone[1], x)
        y_zone[0] = min(y_zone[0], y)
        y_zone[1] = max(y_zone[1], y)
        z_zone[0] = min(z_zone[0], z)
        z_zone[1] = max(z_zone[1], z)
        w_zone[0] = min(w_zone[0], w)
        w_zone[1] = max(w_zone[1], w)

    # Init from data
    size = 0
    for y, line in enumerate(data):
        if size == 0:
            size = len(line) + (nb_iter + 1) * 2
            space = np.zeros([size, size, size, size], bool)
            new_space = np.zeros([size, size, size, size], bool)
        for x, cell in enumerate(line):
            x_shifted = x_zone[0] + x
            y_shifted = y_zone[0] + y
            z_shifted = z_zone[0] + 0
            w_shifted = w_zone[0] + 0
            if cell == '#':
                space[x_shifted, y_shifted, z_shifted, w_shifted] = True
                update_zone(x_shifted, y_shifted, z_shifted, w_shifted)

    # Iterate
    for _ in range(6):
        x_first = x_zone[0] - 1
        x_last = x_zone[1] + 1
        y_first = y_zone[0] - 1
        y_last = y_zone[1] + 1
        z_first = z_zone[0] - 1
        z_last = z_zone[1] + 1
        w_first = w_zone[0] - 1
        w_last = w_zone[1] + 1
        for x in range(x_first, x_last + 1):
            for y in range(y_first, y_last + 1):
                for z in range(z_first, z_last + 1):
                    for w in range(w_first, w_last + 1):
                        nb = neightbour_nb_active(space, x, y, z, w)
                        new_space[x, y, z, w] = ((nb == 3) or
                                                 (space[x, y, z, w] and
                                                  (nb == 2)))
                        if new_space[x, y, z, w]:
                            update_zone(x, y, z, w)

        space = new_space
        new_space = np.zeros([size, size, size, size], bool)

    return nb_active(space)


def main() -> None:
    data = list(map(str.rstrip, sys.stdin.readlines()))
    print(part1(data))
    sys.stdout.flush()
    print(part2(data))


if __name__ == '__main__':
    main()
