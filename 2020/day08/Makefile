# Static analysis and cleaning --- December 1st, 2020
.SUFFIXES:

PROG_SRC = $(sort $(wildcard *.py))
SRC = $(PROG_SRC)


PYTHON      = python3  # https://www.python.org/
PYTHONFLAGS =

PYPY      = pypy3  # https://www.pypy.org/
PYPYFLAGS =


MYPY      = mypy  # http://www.mypy-lang.org/
MYPYFLAGS = # --warn-unused-ignores

PYCODESTYLE      = pycodestyle  # (pep8) https://pypi.org/project/pycodestyle/
PYCODESTYLEFLAGS = --statistics  # --ignore=E501

PYFLAKES      = pyflakes  # https://pypi.org/project/pyflakes/
PYFLAKESFLAGS =

PYLINT      = pylint  # https://www.pylint.org/
PYLINTFLAGS = -j $(JOB) --disable=line-too-long,locally-disabled,missing-module-docstring,missing-function-docstring --good-names=a,b,c,d,e,f,g,h,i,j,k,l,m,n,nb,o,ok,p,q,r,s,t,u,v,w,x,x0,x1,y,y0,y1,z

PYTYPE      = pytype  # https://google.github.io/pytype/
PYTYPEFLAGS = -P $(PWD):$(PYTHONPATH) -k -j $(JOB)


ECHO  = echo
READ  = read
RM    = rm -f
RMDIR = rmdir
SHELL = sh
TEE   = tee



JOB ?= 1  # change this by define new value when start: $ make JOB=3



###
# #
###
.PHONY: all

all:	lintlog



###################
# Static analysis #
###################
.PHONY: lint lintlog mypy pycodestyle pyflakes pylint pytype

lint:	pycodestyle pyflakes pylint pytype mypy

lintlog:
	@$(ECHO) 'Lint ('`date`') of solution.py' | $(TEE) lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYLINT) $(PYLINTFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` ===== | $(TEE) -a lint.log
	-export MYPYPATH=$(PWD):$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log


mypy:
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` =====
	-export MYPYPATH=$(PWD):$(PWD)/tests:$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC)
	@$(ECHO)

pycodestyle:
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` =====
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC)
	@$(ECHO)

pyflakes:
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` =====
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC)
	@$(ECHO)

pylint:
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` =====
	-$(PYLINT) $(PYLINTFLAGS) -f colorized $(SRC)
	@$(ECHO)

pytype:
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` =====
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC)
	@$(ECHO)



#########
# Clean #
#########
.PHONY:	clean cleanLint cleanResult distclean overclean

clean:	cleanResult
	$(RM) -r .mypy_cache
	$(RM) -r .pytype

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean

overclean:	distclean cleanLint
