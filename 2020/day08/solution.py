#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 8: Handheld Halting
https://adventofcode.com/2020/day/8

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 8, 2020
"""

import sys

from typing import List, Tuple


def run(code: List[Tuple[str, int]]) -> Tuple[bool, int]:
    accumulator = 0
    ip = 0
    ips = set()
    while (0 <= ip < len(code)) and (ip not in ips):
        ips.add(ip)

        ope, arg = code[ip]
        if ope == 'acc':
            accumulator += arg
            ip += 1
        elif ope == 'jmp':
            ip += arg
        elif ope == 'nop':
            ip += 1
        else:
            assert False

    assert 0 <= ip <= len(code)

    return (ip == len(code)), accumulator


def main() -> None:
    code = []
    for line in sys.stdin:
        ope, arg_str = line.rstrip().split()
        arg = int(arg_str)
        code.append((ope, arg))

    # Part 1
    is_terminated1, accumulator1 = run(code)
    print(accumulator1)

    assert not is_terminated1

    # Part 2
    is_terminated2 = False
    accumulator2 = None
    for i, keep_ope_arg in enumerate(code):
        ope, arg = keep_ope_arg
        if ope in ('jmp', 'nop'):
            code[i] = (('nop' if ope == 'jmp'
                        else 'jmp'), arg)
            is_terminated2, accumulator2 = run(code)
            if is_terminated2:
                break

            code[i] = keep_ope_arg

    print(accumulator2)


if __name__ == '__main__':
    main()
