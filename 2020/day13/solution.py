#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 13: Shuttle Search
https://adventofcode.com/2020/day/13

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 13, 2020
"""

import math
import sys

from typing import List, Tuple


def lcm(a, b) -> int:
    return a * b // math.gcd(a, b)


def part1(buses: List[int], earliest: int) -> int:
    better = None
    better_diff = sys.maxsize
    for bus in buses:
        if earliest % bus == 0:
            better = bus
            better_diff = 0

            break

        diff = (earliest // bus + 1) * bus - earliest
        if better_diff > diff:
            better_diff = diff
            better = bus

    assert better is not None

    return better * better_diff


def part2(buses_timestamps: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    if len(buses_timestamps) <= 1:
        return buses_timestamps

    step = buses_timestamps[0][0]
    earliest = -step - buses_timestamps[0][1]

    nb = 0
    while nb != 2:
        earliest += step
        nb = 0
        for bus, timestamp in buses_timestamps[:2]:
            if (earliest + timestamp) % bus == 0:
                nb += 1

    bus = lcm(buses_timestamps[0][0], buses_timestamps[1][0])

    return part2([(bus, bus - earliest)] + buses_timestamps[2:])


def main() -> None:
    earliest = int(input())
    buses = []
    buses_timestamps = []
    for line in sys.stdin:
        for i, x in enumerate(line.rstrip().split(',')):
            if x != 'x':
                buses.append(int(x))
                buses_timestamps.append((int(x), i))

    # Part 1
    print(part1(buses, earliest))
    sys.stdout.flush()

    # Part 2
    buses_timestamps.sort()
    bus, earliest = part2(buses_timestamps)[0]
    print(bus - earliest)


if __name__ == '__main__':
    main()
