#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 24: Lobby Layout
https://adventofcode.com/2020/day/24

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 24, 2020
"""

import sys

from typing import List, Tuple


GridT = List[List[bool]]
MovessT = Tuple[Tuple[Tuple[int, int], ...], ...]


OFFSETS = {'e': (-2, 0),
           'se': (-1, -1),
           'sw': (1, -1),
           'w': (2, 0),
           'nw': (1, 1),
           'ne': (-1, 1)}


def compute_size(movess: MovessT) -> Tuple[int, int, int, int]:
    x0 = 0
    y0 = 0
    x1 = 0
    y1 = 0
    for moves in movess:
        x = 0
        y = 0
        for move in moves:
            x += move[0]
            y += move[1]
            x0 = min(x0, x)
            y0 = min(y0, y)
            x1 = max(x1, x)
            y1 = max(y1, y)

    x_init = 0
    if x0 < 0:
        x_init -= x0
        x1 -= x0
        x0 = 0

    y_init = 0
    if y0 < 0:
        y_init -= y0
        y1 -= y0
        y0 = 0

    width = x1 + 1
    height = y1 + 1

    return width + 4, height + 4, x_init + 2, y_init + 2


def create_grid(width: int, height: int) -> GridT:
    assert width > 0
    assert height > 0

    return [[False] * width for _ in range(height)]


def count_black(grid: GridT) -> int:
    nb = 0
    for row in grid:
        nb += row.count(True)

    return nb


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def part1_paint(grid: GridT, x_init: int, y_init: int,
                movess: MovessT) -> None:
    assert 0 < y_init < len(grid) - 1
    assert 0 < x_init < len(grid[0]) - 1

    for moves in movess:
        x = x_init
        y = y_init
        for move in moves:
            x += move[0]
            y += move[1]

            assert 0 <= x < len(grid[0])
            assert 0 <= y < len(grid)

        grid[y][x] = not grid[y][x]


def part2_paint(grid: GridT) -> GridT:
    assert grid

    width = len(grid[0])
    height = len(grid)

    for i in range(100):
        new_grid = create_grid(width + 4, height + 4)

        for y, row in enumerate(grid):
            for x, tile in enumerate(row):
                nb_black = 0
                for x_offset, y_offset in OFFSETS.values():
                    moved_x = x + x_offset
                    moved_y = y + y_offset
                    if ((0 <= moved_x < width) and (0 <= moved_y < height) and
                            grid[moved_y][moved_x]):
                        nb_black += 1

                if tile:  # black
                    new_grid[y + 2][x + 2] = (1 <= nb_black <= 2)
                else:     # white
                    new_grid[y + 2][x + 2] = (nb_black == 2)

        grid = new_grid
        width += 4
        height += 4

        if (i + 1 <= 10) or ((i + 1) % 10 == 0):
            log('Day', i + 1, count_black(grid))

    return grid


def read_data() -> MovessT:
    list_movess = []
    for line in sys.stdin:
        line = line.rstrip()
        list_moves: List[Tuple[int, int]] = []
        while line:
            move = OFFSETS.get(line[0])
            if move is not None:
                direction, line = line[0], line[1:]
            else:
                direction, line = line[0:2], line[2:]
                move = OFFSETS[direction]

            list_moves.append(move)
        list_movess.append(tuple(list_moves))

    return tuple(list_movess)


def main() -> None:
    movess = read_data()
    width, height, x_init, y_init = compute_size(movess)

    grid = create_grid(width, height)

    # Part 1
    part1_paint(grid, x_init, y_init, movess)
    print(count_black(grid))
    sys.stdout.flush()

    # Part 2
    grid = part2_paint(grid)
    print(count_black(grid))


if __name__ == '__main__':
    main()
