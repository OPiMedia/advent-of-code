#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 6: Custom Customs
https://adventofcode.com/2020/day/6

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 6, 2020
"""

import sys


def main() -> None:
    all_letters = 'abcdefghijklmnopqrstuvwxyz'

    questions = set()
    question_sum_nb = 0

    group_questions = set(all_letters)
    group_question_sum_nb = 0

    for line in sys.stdin:
        line = line.rstrip()
        if line:
            qs = set(line)
            questions |= qs
            group_questions &= qs
        else:
            question_sum_nb += len(questions)
            group_question_sum_nb += len(group_questions)

            questions = set()
            group_questions = set(all_letters)

    if questions:
        question_sum_nb += len(questions)
        group_question_sum_nb += len(group_questions)

    # Part 1
    print(question_sum_nb)

    # Part 2
    print(group_question_sum_nb)


if __name__ == '__main__':
    main()
