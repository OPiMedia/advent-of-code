#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 14: Docking Data
https://adventofcode.com/2020/day/14

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 14, 2020
"""

import re
import sys

from typing import List, Tuple, Union

InstT = Union[str, Tuple[int, int]]
ProgramT = Tuple[InstT, ...]


def part1(program: ProgramT) -> int:
    memory = dict()
    for inst in program:
        if isinstance(inst, str):
            mask_and = int(inst.replace('X', '1'), 2)
            mask_or = int(inst.replace('X', '0'), 2)
        else:
            address, value = inst
            memory[address] = (value & mask_and) | mask_or

    return sum(memory.values())


def part2(program: ProgramT) -> int:
    memory = dict()

    def memory_set(address: int, value: int, mask: str, i: int):
        assert 0 <= i <= len(mask)

        if i == len(mask):
            memory[address] = value
        elif mask[i] != 'X':
            memory_set(address, value, mask, i + 1)
        else:
            bit = 2**i
            memory_set(address | bit, value, mask, i + 1)
            memory_set(address & ((2**36 - 1) ^ bit), value, mask, i + 1)

    for inst in program:
        if isinstance(inst, str):
            mask = ''.join(reversed(inst))
            mask_or = int(inst.replace('X', '1'), 2)
        else:
            address, value = inst
            memory_set(address | mask_or, value, mask, 0)

    return sum(memory.values())


def read_data() -> ProgramT:
    re_mask = re.compile('mask = (.*)')
    re_address = re.compile(r'mem\[(.*)\] = (.*)')

    program: List[InstT] = []
    for line in sys.stdin:
        line = line.rstrip()
        match = re_mask.match(line)
        if match:
            mask = match.group(1)
            program.append(mask)

            assert len(mask) == 36
        else:
            match = re_address.match(line)

            assert match

            address = int(match.group(1))
            value = int(match.group(2))
            program.append((address, value))

    return tuple(program)


def main() -> None:
    program = read_data()

    # Part 1
    s = part1(program)
    print(s)

    # Part 2
    if program[0] == 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X':
        # input0 data is too "big" to be solve with this algorithm
        print('?')
    else:
        s = part2(program)
        print(s)


if __name__ == '__main__':
    main()
