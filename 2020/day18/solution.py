#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 18: Operation Order
https://adventofcode.com/2020/day/18

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 18, 2020
"""

import re
import sys


SUBEXPR_RE = re.compile(r'(.*)\(([^()]+)\)(.*)$')


def compute(a: int, ope: str, b: int) -> int:
    if ope == '+':
        return a + b

    assert ope == '*'

    return a * b


def evaluate1(expr: str) -> int:
    assert expr

    if expr[-1] == ')':
        i = len(expr) - 1
        stack = [')']
        while stack:
            i -= 1
            if expr[i] == '(':
                stack.pop()
            elif expr[i] == ')':
                stack.append(')')

        if i == 0:
            return evaluate1(expr[1:-1])

        assert expr[i - 3] == ' '
        assert expr[i - 1] == ' '

        a = evaluate1(expr[:i - 3])
        ope = expr[i - 2]
        b = evaluate1(expr[i + 1:-1])
    else:
        pieces = expr.split(' ')
        if len(pieces) > 3:
            pieces[-3] = ' '.join(pieces[:-2])

            del pieces[:-3]

        if len(pieces) == 1:
            return int(pieces[0])

        assert len(pieces) == 3

        a = evaluate1(pieces[0])
        ope = pieces[1]
        b = int(pieces[2])

    return compute(a, ope, b)


def evaluate2(expr: str) -> int:
    """Dirty solution :-)"""
    assert expr

    if ('+' not in expr) or ('*' not in expr):
        if ('+' not in expr) and ('*' not in expr):
            return int(expr)

        return evaluate1(expr)

    match = SUBEXPR_RE.match(expr)
    if match:
        return evaluate2(match.group(1) + str(evaluate2(match.group(2))) +
                         match.group(3))

    pieces = expr.split('*', 1)

    assert len(pieces) == 2, pieces

    a = evaluate2(pieces[0].rstrip())
    b = evaluate2(pieces[1].lstrip())

    return compute(a, '*', b)


def main() -> None:
    exprs = list(map(str.rstrip, sys.stdin.readlines()))

    # Part 1
    n = 0
    for expr in exprs:
        n += evaluate1(expr)

    print(n)
    sys.stdout.flush()

    # Part 2
    n = 0
    for expr in exprs:
        n += evaluate2(expr)

    print(n)


if __name__ == '__main__':
    main()
