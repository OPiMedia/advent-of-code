#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 4: Passport Processing
https://adventofcode.com/2020/day/4

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 4, 2020
"""

import re
import sys

from typing import Dict


def is_valid1(passport: Dict[str, str]) -> bool:
    length = len(passport)

    return (length == 8) or ((length == 7) and ('cid' not in passport))


def is_valid2(passport: Dict[str, str]) -> bool:
    if not is_valid1(passport):
        return False

    if not is_valid_date(passport['byr'], 1920, 2002):
        return False
    if not is_valid_date(passport['iyr'], 2010, 2020):
        return False
    if not is_valid_date(passport['eyr'], 2020, 2030):
        return False

    match = re.match('([0-9]+)(cm|in)$', passport['hgt'])
    if not match:
        return False
    hgt = int(match.group(1))
    if match.group(2) == 'cm':
        if not 150 <= hgt <= 193:
            return False
    else:
        if not 59 <= hgt <= 76:
            return False

    if not re.match('#[0-9a-f]{6}$', passport['hcl']):
        return False

    if passport['ecl'] not in ('amb', 'blu', 'brn', 'gry',
                               'grn', 'hzl', 'oth'):
        return False

    if not re.match('[0-9]{9}$', passport['pid']):
        return False

    return True


def is_valid_date(date_str: str, date_from: int, date_to: int) -> bool:
    if len(date_str) != 4:
        return False

    try:
        date = int(date_str)
    except ValueError:
        return False

    return date_from <= date <= date_to


def main() -> None:
    """
    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)
    """
    passport = dict()
    nb1 = 0
    nb2 = 0

    for line in sys.stdin:
        line = line.rstrip()
        if line:
            pieces = line.split()
            for piece in pieces:
                key, value = piece.split(':')
                passport[key] = value
        else:
            if is_valid1(passport):
                nb1 += 1
            if is_valid2(passport):
                nb2 += 1
            passport = dict()

    if is_valid1(passport):
        nb1 += 1
    if is_valid2(passport):
        nb2 += 1

    # Part 1
    print(nb1)

    # Part 2
    print(nb2)


if __name__ == '__main__':
    main()
