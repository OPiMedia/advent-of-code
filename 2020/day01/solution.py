#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 1: Report Repair
https://adventofcode.com/2020/day/1

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 2, 2020
"""

import bisect
import sys


def main() -> None:
    expenses = list(map(int, sys.stdin.readlines()))

    expenses.sort()

    # Part 1
    for i, a in enumerate(expenses):
        j = bisect.bisect_left(expenses, 2020 - a, lo=i + 1)
        if (j != len(expenses)) and (expenses[j] == 2020 - a):
            print(a * expenses[j])

    # Part 2
    for i, a in enumerate(expenses):
        for j, b in enumerate(expenses[i + 1:]):
            s = a + b
            if a + b > 2020:
                break

            k = bisect.bisect_left(expenses, 2020 - s, lo=j + 1)
            if (k != len(expenses)) and (expenses[k] == 2020 - s):
                print(a * b * expenses[k])


if __name__ == '__main__':
    main()
