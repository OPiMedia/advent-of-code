#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 5: Binary Boarding
https://adventofcode.com/2020/day/5

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 5, 2020
"""

import sys


def search(nb: int, moves: str) -> int:
    first = 0
    after_last = nb

    for move in moves:
        middle = first + (after_last - first) // 2
        if move in 'FL':  # first hald
            after_last = middle
        else:  # second half
            first = middle

    return first


def to_id(row: int, column: int) -> int:
    return row * 8 + column


def main() -> None:
    seat_ids = []
    for line in sys.stdin:
        line = line.rstrip()

        assert len(line) == 10

        row = search(128, line[:7])
        column = search(8, line[7:])
        seat_id = to_id(row, column)
        seat_ids.append(seat_id)

        # print(row, column, seat_id, file=sys.stderr)

    # Part 1
    print(max(seat_ids))

    # Part 2
    free_ids = (frozenset(range(to_id(0, 0), to_id(127, 7) + 1)) -
                frozenset(seat_ids))
    for free_id in free_ids:
        if not frozenset((free_id - 1, free_id + 1)) & free_ids:
            print(free_id)


if __name__ == '__main__':
    main()
