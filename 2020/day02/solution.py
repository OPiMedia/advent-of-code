#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 2: Password Philosophy
https://adventofcode.com/2020/day/2

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 2, 2020
"""

import re
import sys


def xor(a: bool, b: bool) -> bool:
    return (a and not b) or (not a and b)


def main() -> None:
    nb1 = 0
    nb2 = 0
    for line in sys.stdin:
        low_str, high_str, letter, password = re.split('[-: ]+', line.rstrip())
        low = int(low_str)
        high = int(high_str)

        occurence = 0
        for c in password:
            if c == letter:
                occurence += 1
                if occurence > high:
                    break
        if low <= occurence <= high:
            nb1 += 1

        if xor(password[low - 1] == letter, password[high - 1] == letter):
            nb2 += 1

    # Part 1
    print(nb1)

    # Part 2
    print(nb2)


if __name__ == '__main__':
    main()
