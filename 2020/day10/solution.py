#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 10: Adapter Array
https://adventofcode.com/2020/day/10

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 10, 2020
"""

import sys

from typing import List, Optional

MEMO: List[Optional[int]] = [None, None]


def count(joltages: List[int], first: int) -> int:
    assert 0 <= first < len(joltages)

    if first + 1 == len(joltages):
        return 1

    nb = MEMO[first]
    if nb is not None:
        return nb

    assert joltages[first] + 3 >= joltages[first + 1]

    i = first + 1
    nb = 0
    while (i < len(joltages)) and (joltages[first] + 3 >= joltages[i]):
        nb += count(joltages, i)
        i += 1

    MEMO[first] = nb

    return nb


def main() -> None:
    joltages = [0]
    for line in sys.stdin:
        joltages.append(int(line.rstrip()))
        MEMO.append(None)

    joltages.sort()
    joltages.append(joltages[-1] + 3)

    assert len(MEMO) == len(joltages)

    differences = [0] * 4
    for i, joltage in enumerate(joltages[:-1]):
        difference = joltages[i + 1] - joltage

        assert 1 <= difference <= 3

        differences[difference] += 1

    # Part 1
    print(differences[1] * differences[3])

    # Part 2
    print(joltages, file=sys.stderr)
    nb = count(joltages, 0)
    print(nb)


if __name__ == '__main__':
    main()
