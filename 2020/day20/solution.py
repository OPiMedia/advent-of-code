#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 20: Jurassic Jigsaw
https://adventofcode.com/2020/day/20

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 26, 2020
"""

import copy
import math
import re
import sys

from typing import Dict, List, Tuple

import numpy as np  # type: ignore


TOP = 0
RIGHT = 1
BOTTOM = 2
LEFT = 3

FCT_FLIPS = (lambda a: a,
             np.fliplr,
             np.flipud,
             lambda a: np.flipud(np.fliplr(a)))


class Border:
    def __init__(self, border: Tuple[bool, ...]) -> None:
        assert border

        self._border = border
        self._nb_true = border.count(True)

    def __eq__(self, other) -> bool:
        return (isinstance(other, Border) and
                (self._nb_true == other._nb_true) and
                (self._border == other._border))

    def __hash__(self) -> int:
        return hash((self._nb_true, self._border))

    def __str__(self) -> str:
        return ''.join(map(bool_to_str, self._border))

    def reverse(self) -> 'Border':
        border = copy.copy(self)
        border._border = tuple(reversed(border._border))  # pylint: disable=protected-access  # noqa

        return border


class Grid:
    def __init__(self) -> None:
        def read_data() -> Tuple[Tile, ...]:
            tiles: List[Tile] = []

            line = ''
            while line == '':
                tile_id = int(input()[:-1].split()[1])
                tile_strs = []
                for line in sys.stdin:
                    line = line.rstrip()
                    if line == '':
                        break

                    tile_strs.append(line)

                tile = Tile(tile_id, tuple(tile_strs))
                tiles.append(tile)

            return tuple(tiles)

        self._all_tiles = read_data()
        self._grid_size = round(math.sqrt(len(self._all_tiles)))

        assert self._grid_size ** 2 == len(self._all_tiles)

        histo: Dict[Border, int] = dict()

        def search_border_and_corner_tiles() -> Tuple[Tuple['Tile', ...],
                                                      Tuple['Tile', ...],
                                                      Tuple['Tile', ...]]:
            borders = []
            corners = []
            inners = []

            for tile in self._all_tiles:
                for border in tile.all_borders():
                    histo[border] = histo.get(border, 0) + 1

            for tile in self._all_tiles:
                nb = 0
                for border in tile.all_borders():
                    if histo[border] == 1:
                        nb += 1

                if nb == 1 * 2:  # twice because the border and its reverse
                    borders.append(tile)
                elif nb == 2 * 2:  # twice because the border and its reverse
                    corners.append(tile)
                else:
                    assert nb == 0

                    inners.append(tile)

            assert len(corners) == 4

            return tuple(inners), tuple(borders), tuple(corners)

        self._inner_tiles, self._border_tiles, self._corner_tiles = \
            search_border_and_corner_tiles()

        self._grid: Tuple[List[Tile], ...] = \
            tuple([Tile(0, tuple())] * self._grid_size
                  for _ in range(self._grid_size))

        border_tiles = list(self._border_tiles)
        corner_tiles = list(self._corner_tiles[1:])
        inner_tiles = list(self._inner_tiles)

        def set_top_left_corner_and_right() -> None:
            example_top = Border(tuple(map(lambda c: c == '#', '#...##.#..')))

            for a_sides in self._corner_tiles[0].all_sides_combinations():
                if ((histo[a_sides.border(LEFT)] == 1) and
                        (histo[a_sides.border(TOP)] == 1)):
                    for i, tile in enumerate(border_tiles):
                        for b_sides in tile.all_sides_combinations():
                            if ((b_sides.border(LEFT) == a_sides.border(RIGHT))
                                    and (histo[b_sides.border(TOP)] == 1)):
                                if a_sides.border(LEFT) == example_top:
                                    # Avoid this possibility to have exact
                                    # same orientation that the example
                                    continue

                                self._corner_tiles[0].set_combination(
                                    LEFT, a_sides.border(LEFT),
                                    TOP, a_sides.border(TOP))
                                self._grid[0][0] = self._corner_tiles[0]

                                tile.set_combination(
                                    LEFT, a_sides.border(RIGHT),
                                    TOP, b_sides.border(TOP))
                                self._grid[0][1] = tile

                                assert border_tiles[i] == tile

                                del border_tiles[i]

                                return

            assert False

        set_top_left_corner_and_right()

        def set_top_border(x: int) -> None:
            a_right = self._grid[0][x - 1].border(RIGHT)
            for i, tile in enumerate(border_tiles):
                for b_sides in tile.all_sides_combinations():
                    if ((b_sides.border(LEFT) == a_right) and
                            (histo[b_sides.border(TOP)] == 1)):
                        tile.set_combination(LEFT, a_right,
                                             TOP, b_sides.border(TOP))
                        self._grid[0][x] = tile

                        assert border_tiles[i] == tile

                        del border_tiles[i]

                        return

            assert False

        for x in range(2, self._grid_size - 1):
            set_top_border(x)

        def set_top_right_corner() -> None:
            a_right = self._grid[0][self._grid_size - 2].border(RIGHT)
            for i, tile in enumerate(corner_tiles):
                for b_sides in tile.all_sides_combinations():
                    if ((b_sides.border(LEFT) == a_right) and
                            (histo[b_sides.border(TOP)] == 1)):
                        assert histo[b_sides.border(RIGHT)] == 1

                        tile.set_combination(LEFT, a_right,
                                             TOP, b_sides.border(TOP))
                        self._grid[0][self._grid_size - 1] = tile

                        assert corner_tiles[i] == tile

                        del corner_tiles[i]

                        return

            assert False

        set_top_right_corner()

        def set_left_border(y: int) -> None:
            a_bottom = self._grid[y - 1][0].border(BOTTOM)
            for i, tile in enumerate(border_tiles):
                for b_sides in tile.all_sides_combinations():
                    if ((b_sides.border(TOP) == a_bottom) and
                            (histo[b_sides.border(LEFT)] == 1)):
                        tile.set_combination(TOP, a_bottom,
                                             LEFT, b_sides.border(LEFT))
                        self._grid[y][0] = tile

                        assert border_tiles[i] == tile

                        del border_tiles[i]

                        return

            assert False

        for y in range(1, self._grid_size - 1):
            set_left_border(y)

        def set_bottom_right_corner() -> None:
            a_bottom = self._grid[self._grid_size - 2][0].border(BOTTOM)
            for i, tile in enumerate(corner_tiles):
                for b_sides in tile.all_sides_combinations():
                    if ((b_sides.border(TOP) == a_bottom) and
                            (histo[b_sides.border(LEFT)] == 1)):
                        assert histo[b_sides.border(BOTTOM)] == 1

                        tile.set_combination(TOP, a_bottom,
                                             LEFT, b_sides.border(LEFT))
                        self._grid[self._grid_size - 1][0] = tile

                        assert corner_tiles[i] == tile

                        del corner_tiles[i]

                        return

            assert False

        set_bottom_right_corner()

        def set_row(x: int, y: int) -> None:
            tiles = inner_tiles
            if (x == self._grid_size - 1) or (y == self._grid_size - 1):
                tiles = (corner_tiles
                         if ((x == self._grid_size - 1) and
                             (y == self._grid_size - 1))
                         else border_tiles)
            a_bottom = self._grid[y - 1][x].border(BOTTOM)
            a_right = self._grid[y][x - 1].border(RIGHT)
            for i, tile in enumerate(tiles):
                for b_sides in tile.all_sides_combinations():
                    if ((b_sides.border(LEFT) == a_right) and
                            (b_sides.border(TOP) == a_bottom)):
                        tile.set_combination(LEFT, a_right,
                                             TOP, a_bottom)
                        self._grid[y][x] = tile

                        assert tiles[i] == tile

                        del tiles[i]

                        return

            assert False

        for y in range(1, self._grid_size):
            for x in range(1, self._grid_size):
                set_row(x, y)

        assert not inner_tiles
        assert not border_tiles
        assert not corner_tiles

    def __repr__(self) -> str:
        return '\n'.join(' '.join(str(tile.tile_id()) for tile in row)
                         for row in self._grid)

    def __str__(self) -> str:
        seq = []
        for row in self._grid:
            seq.append('')
            for y in range(row[0].height()):
                line = []
                for tile in row:
                    line.append(''.join(map(bool_to_str, tile.row(y))))

                seq.append(' '.join(line))

        return '\n'.join(seq[1:])

    def all_tiles(self) -> Tuple['Tile', ...]:
        return self._all_tiles

    def border_tiles(self) -> Tuple['Tile', ...]:
        return self._border_tiles

    def concatenate(self) -> Tuple[str, ...]:
        seq = []
        for row in self._grid:
            for y in range(1, row[0].height() - 1):
                line = []
                for tile in row:
                    line.append(''.join(map(bool_to_str, tile.row(y)[1:-1])))

                seq.append(''.join(line))

        return tuple(seq)

    def corner_tiles(self) -> Tuple['Tile', ...]:
        return self._corner_tiles

    def inner_tiles(self) -> Tuple['Tile', ...]:
        return self._inner_tiles


class Sides:
    def __init__(self, top: Border, right: Border,
                 bottom: Border, left: Border) -> None:
        self._sides = (top, right, bottom, left)

    def __eq__(self, other) -> bool:
        return self._sides == other._sides

    def __hash__(self) -> int:
        return hash(self._sides)

    def __str__(self) -> str:
        return '({})'.format(', '.join(map(str, self._sides)))

    def border(self, side: int) -> Border:
        assert side in (TOP, RIGHT, BOTTOM, LEFT)

        return self._sides[side]

    def flip_horizontal(self) -> 'Sides':
        sides = copy.copy(self)
        sides._sides = (sides._sides[2], sides._sides[1].reverse(),  # pylint: disable=protected-access  # noqa
                        sides._sides[0], sides._sides[3].reverse())  # pylint: disable=protected-access  # noqa

        return sides

    def flip_vertical(self) -> 'Sides':
        sides = copy.copy(self)
        sides._sides = (sides._sides[0].reverse(), sides._sides[3],  # pylint: disable=protected-access  # noqa
                        sides._sides[2].reverse(), sides._sides[1])  # pylint: disable=protected-access  # noqa

        return sides

    def rotate(self) -> 'Sides':
        sides = copy.copy(self)
        sides._sides = (sides._sides[3].reverse(),  # pylint: disable=protected-access  # noqa
                        sides._sides[0],  # pylint: disable=protected-access
                        sides._sides[1].reverse(),  # pylint: disable=protected-access  # noqa
                        sides._sides[2])  # pylint: disable=protected-access

        return sides


class Tile:
    def __init__(self, tile_id: int, tile: Tuple[str, ...]) -> None:
        assert tile_id >= 0, tile_id

        self._id = tile_id
        self._array = tuple(tuple(map(lambda char: char == '#', row))
                            for row in tile)

    def __eq__(self, other) -> bool:
        return (self.tile_id() > 0) and (self.tile_id() == other.tile_id())

    def __str__(self) -> str:
        return '\n'.join(''.join(map(bool_to_str, row))
                         for row in self._array)

    def all_borders(self) -> Tuple[Border, ...]:
        borders = [self.border(i) for i in range(4)]
        for border in tuple(borders):
            borders.append(border.reverse())

        assert len(borders) == 8
        assert len(set(borders)) == 8

        return tuple(borders)

    def all_sides_combinations(self) -> Tuple[Sides, ...]:
        combs = [Sides(self.border(TOP), self.border(RIGHT),
                       self.border(BOTTOM), self.border(LEFT))]

        for _ in range(3):
            combs.append(combs[-1].rotate())

        for sides in tuple(combs):
            combs.append(sides.flip_horizontal())
            combs.append(sides.flip_vertical())
            combs.append(sides.flip_horizontal().flip_vertical())

        assert len(combs) == 16
        assert len(set(combs)) == 8

        return tuple(set(combs))

    def border(self, side: int) -> Border:
        assert side in (TOP, RIGHT, BOTTOM, LEFT)
        assert self._array

        if side == TOP:
            return Border(self._array[0])
        if side == RIGHT:
            return Border(tuple(row[-1] for row in self._array))
        if side == BOTTOM:
            return Border(self._array[-1])

        return Border(tuple(row[0] for row in self._array))

    def height(self) -> int:
        return len(self._array)

    def row(self, y: int) -> Tuple[bool, ...]:
        return self._array[y]

    def set_combination(self, side1: int, border1: Border,
                        side2: int, border2: Border) -> None:
        assert side1 in (TOP, RIGHT, BOTTOM, LEFT)
        assert side2 in (TOP, RIGHT, BOTTOM, LEFT)
        assert side1 != side2
        assert border1 != border2
        assert self._array

        np_array = np.array(self._array)
        tile = copy.copy(self)
        tile._id = -1  # pylint: disable=protected-access
        for i in range(4):
            if i != 0:
                np_array = np.rot90(np_array)
            for fct_flip in FCT_FLIPS:
                np_flipped = fct_flip(np_array)
                tile._array = tuple(map(tuple, np_flipped))  # pylint: disable=protected-access  # noqa
                if ((tile.border(side1) == border1) and
                        (tile.border(side2) == border2)):
                    self._array = tile._array  # pylint: disable=protected-access  # noqa

                    return

        assert False

    def sides(self) -> Sides:
        return Sides(self.border(TOP), self.border(RIGHT),
                     self.border(BOTTOM), self.border(LEFT))

    def tile_id(self) -> int:
        return self._id

    def width(self) -> int:
        return len(self._array[0])


def bool_to_str(b: bool) -> str:
    return ('#' if b
            else '.')


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def main() -> None:
    # Part 1
    grid = Grid()

    # log(repr(grid))

    corner_product = 1
    for tile in grid.corner_tiles():
        corner_product *= tile.tile_id()

    print(corner_product)

    # Part 2
    image = grid.concatenate()
    np_image = np.array(tuple(tuple(line) for line in image))
    monster = ('                  # ',
               '#    ##    ##    ###',
               ' #  #  #  #  #  #   ')

    count_sharp = sum(map(lambda line: line.count('#'), image))
    count_sharp_monster = sum(map(lambda line: line.count('#'), monster))

    re_monster = tuple(map(lambda line: re.compile(line.replace(' ', '.')),
                           monster))
    for i in range(4):
        if i != 0:
            np_image = np.rot90(np_image)

        for fct_flip in FCT_FLIPS:
            np_flipped = fct_flip(np_image)
            lines = tuple(map(''.join, np_flipped))

            nb = 0
            for y, row in enumerate(lines):
                if (1 <= y < len(lines) - 1) and re_monster[1].search(row):
                    for k in range(len(row)):
                        found = True
                        for j in range(3):
                            if not re_monster[j].match(lines[y - 1 + j][k:]):
                                found = False

                                break

                            if not found:
                                break

                        if found:
                            nb += 1

            if nb != 0:
                print(count_sharp - count_sharp_monster * nb)

                return


if __name__ == '__main__':
    main()
