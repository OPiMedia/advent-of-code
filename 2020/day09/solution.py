#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 9: Encoding Error
https://adventofcode.com/2020/day/9

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 9, 2020
"""

import sys

from typing import Dict


def main() -> None:
    # Read preamble
    all_numbers = []
    numbers = []
    for _ in range(25):
        n = int(input())
        all_numbers.append(n)
        numbers.append(n)

    # Computes sums
    sums: Dict[int, int] = dict()

    def add_sum(s: int) -> None:
        sums[s] = sums.get(s, 0) + 1

    def remove_sum(s: int) -> None:
        assert s in sums
        assert sums[s] >= 1

        if sums[s] == 1:
            del sums[s]
        else:
            sums[s] -= 1

    for i, a in enumerate(numbers):
        for b in numbers[i + 1:]:
            add_sum(a + b)

    # Part 1: Check next numbers
    invalid = None
    for line in sys.stdin:
        n = int(line.rstrip())
        all_numbers.append(n)
        if n not in sums:  # invalid
            invalid = n

            break

        # Remove sums with first number
        first_n, numbers = numbers[0], numbers[1:]
        for b in numbers:
            remove_sum(first_n + b)

        # Add sums with new number
        for b in numbers:
            add_sum(n + b)
        numbers.append(n)

    print(invalid)

    assert isinstance(invalid, int)

    # Part 2: Search sum by contiguous numbers
    first = 0
    last = 0
    while invalid != 0:
        invalid -= all_numbers[last]
        last += 1

        while invalid < 0:
            invalid += all_numbers[first]
            first += 1

    contiguous = all_numbers[first:last + 1]
    print(min(contiguous) + max(contiguous))


if __name__ == '__main__':
    main()
