#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 22: Crab Combat
https://adventofcode.com/2020/day/22

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 22, 2020
"""

import sys

from typing import Tuple


def game(player1: Tuple[int, ...],
         player2: Tuple[int, ...]) -> Tuple[Tuple[int, ...], Tuple[int, ...]]:
    list1 = list(player1)
    list2 = list(player2)

    while list1 and list2:
        card1 = list1.pop(0)
        card2 = list2.pop(0)

        if card1 > card2:
            list1.extend((card1, card2))
        else:
            list2.extend((card2, card1))

    return tuple(list1), tuple(list2)


def game_recursive(player1: Tuple[int, ...], player2: Tuple[int, ...]) \
        -> Tuple[Tuple[int, ...], Tuple[int, ...]]:
    already = set()

    list1 = list(player1)
    list2 = list(player2)

    while list1 and list2:
        player1_player2 = (tuple(list1), tuple(list1))
        if player1_player2 in already:
            return tuple(list1 + list2), tuple()

        already.add(player1_player2)

        card1 = list1.pop(0)
        card2 = list2.pop(0)

        if (card1 <= len(list1)) and (card2 <= len(list2)):
            player1, player2 = game_recursive(tuple(list1[:card1]),
                                              tuple(list2[:card2]))
            if not player2:
                list1.extend((card1, card2))
            else:
                list2.extend((card2, card1))
        else:
            if card1 > card2:
                list1.extend((card1, card2))
            else:
                list2.extend((card2, card1))

    return tuple(list1), tuple(list2)


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def read_player() -> Tuple[int, ...]:
    seq = []
    line = input()
    for line in sys.stdin:
        line = line.rstrip()
        if line == '':
            break

        seq.append(int(line))

    return tuple(seq)


def score(cards: Tuple[int, ...]) -> int:
    return sum(card * (i + 1) for i, card in enumerate(reversed(cards)))


def main() -> None:
    player1 = tuple(read_player())
    player2 = tuple(read_player())

    # Part 1
    end_player1, end_player2 = game(player1, player2)
    print(score(end_player1 if end_player1
                else end_player2))

    sys.stdout.flush()

    # Part 2
    end_player1, end_player2 = game_recursive(player1, player2)
    print(score(end_player1 if end_player1
                else end_player2))


if __name__ == '__main__':
    main()
