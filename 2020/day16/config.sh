# Config file for run.sh and foreach.sh (for solution program in Python)
# See *List of configuration variables* in
# https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/

INPUT_PATTERN='data/input/input*'    # pattern of input files
OUTPUT_PATTERN='data/correct/output*'  # pattern of corresponding correct output files

# VM=''  # virtual machine, to run PROG
# VM_FLAGS=()

# ANALYZER=''  # dynamic analyzer, enabled by -z option
# ANALYZER_FLAGS=()

PROG='./solution.py'  # main program
PROG_FLAGS=()

PROG_SRC='solution.py'  # source code

# INPUT_HEAD=0   # max number of lines to print, for input file
RESULT_HEAD=0  # max number of lines to print, for result file
OUTPUT_HEAD=0  # max number of lines to print, for output file

# MAKE_RUN=0  # if 0 then run make step

# DIFF='sdiff'      # side by side differences
DIFF='colordiff'  # color diff
DIFF_FLAGS=()

# DIFF_VIEW='kdiff3'  # differences viewer, enabled by -v or -V options
# DIFF_VIEW_FLAGS=()
