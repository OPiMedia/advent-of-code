#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 16: Ticket Translation
https://adventofcode.com/2020/day/16

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 16, 2020
"""

import re
import sys


def main() -> None:
    re_rule = re.compile(r'(.+): (\d+)-(\d+) or (\d+)-(\d+)')

    rules = dict()
    for line in sys.stdin:
        match = re_rule.match(line.rstrip())
        if not match:
            break

        rules[match.group(1)] = ((int(match.group(2)), int(match.group(3))),
                                 (int(match.group(4)), int(match.group(5))))

    assert input() == 'your ticket:'

    ticket = tuple(map(int, input().split(',')))

    assert input() == ''
    assert input() == 'nearby tickets:'

    nearbies = []
    for line in sys.stdin:
        nearby = tuple(map(int, line.rstrip().split(',')))
        nearbies.append(nearby)

    # Part 1
    rate = 0
    valids = []  # for part 2
    for nearby in nearbies:
        nearby_is_valid = True
        for n in nearby:
            is_invalid = True
            for rule, constraints in rules.items():
                for a, b in constraints:
                    if a <= n <= b:
                        is_invalid = False

            if is_invalid:
                rate += n
                nearby_is_valid = False

        if nearby_is_valid:
            valids.append(nearby)

    print(rate)

    # Part 2
    uses = [set(rules.keys()) for _ in range(len(ticket))]

    def clean(i: int):
        identified = uses[i].pop()
        for j, use in enumerate(uses):
            if identified in use:
                use.discard(identified)
                if len(use) == 1:
                    clean(j)
        uses[i].add(identified)

    for nearby in valids:
        for i, n in enumerate(nearby):
            for rule, ((a0, b0), (a1, b1)) in rules.items():
                if not ((a0 <= n <= b0) or (a1 <= n <= b1)):
                    uses[i].discard(rule)

                    assert uses[i]

                    if len(uses[i]) == 1:
                        clean(i)

    product = 1
    for i, use in enumerate(uses):
        if use.pop().startswith('departure'):
            product *= ticket[i]

    print(product)


if __name__ == '__main__':
    main()
