#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 15: Rambunctious Recitation
https://adventofcode.com/2020/day/15

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 15, 2020
"""

import sys

from typing import Dict, Tuple


def main() -> None:
    seq = tuple(map(int, input().split(',')))
    tourss: Dict[int, Tuple[int, ...]] = dict()
    last_n = -1
    for i, n in enumerate(seq):
        tourss[n] = tourss.get(n, tuple()) + (i + 1, )
        last_n = n

    for tour in range(len(seq) + 1, 30000000 + 1):
        assert last_n in tourss

        tours = tourss[last_n]

        assert 1 <= len(tours) <= 2

        new_n = (tours[1] - tours[0] if len(tours) == 2
                 else 0)

        tourss[new_n] = tourss.get(new_n, tuple())[-1:] + (tour, )

        last_n = new_n

        # Part 1
        if tour == 2020:
            print(last_n)
            sys.stdout.flush()

    # Part 2
    print(last_n)


if __name__ == '__main__':
    main()
