#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 7: Handy Haversacks
https://adventofcode.com/2020/day/7

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 7, 2020
"""

import re
import sys


from typing import Dict


def print_rules(rules: Dict[str, Dict[str, int]]) -> None:
    for color, contains in sorted(rules.items()):
        print(color, contains, file=sys.stderr)
    print(file=sys.stderr)


def main() -> None:
    rules = dict()
    for line in sys.stdin:
        line = line.rstrip()

        match = re.match('(.+) bags contain (.+).', line)

        assert match

        color = match.group(1)
        contains = dict()
        if match.group(2) != 'no other bags':
            pieces = match.group(2).split(', ')
            for piece in pieces:
                match = re.match('([0-9]+) (.+) bags?', piece)

                assert match

                contains[match.group(2)] = int(match.group(1))

        rules[color] = contains

    # print_rules(rules)

    # Part 1
    golds = {color: set(contains) for color, contains in rules.items()}

    modified = True
    while modified:
        modified = False
        for color, subcolors in tuple(golds.items()):
            size = len(golds[color])
            for subcolor in tuple(subcolors):
                golds[color] |= golds[subcolor]
            modified = modified or (size != len(golds[color]))

    nb = len([None for subcolors in golds.values()
              if 'shiny gold' in subcolors])
    print(nb)

    # Part 2
    def recursive_nb(color: str) -> int:
        total_nb = 1
        for subcolor, nb in rules[color].items():
            total_nb += nb * recursive_nb(subcolor)

        return total_nb

    nb = recursive_nb('shiny gold') - 1
    print(nb)


if __name__ == '__main__':
    main()
