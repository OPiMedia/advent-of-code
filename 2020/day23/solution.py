#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Advent of Code 2020 - Day 23: Crab Cups
https://adventofcode.com/2020/day/23

:license: GPLv3 --- Copyright (C) 2020 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 23, 2020
"""

import sys

from typing import List, Tuple


class Item:
    def __init(self) -> None:
        self._prev: 'Item' = self  # pylint: disable=attribute-defined-outside-init  # noqa
        self._value = 0  # pylint: disable=attribute-defined-outside-init
        self._next: 'Item' = self  # pylint: disable=attribute-defined-outside-init  # noqa

    def get_next(self) -> 'Item':
        return self._next  # type: ignore

    def get_prev(self) -> 'Item':
        return self._prev

    def get_value(self) -> int:
        return self._value  # type: ignore

    def set_next(self, next_item: 'Item') -> None:
        self._next = next_item  # pylint: disable=attribute-defined-outside-init  # noqa

    def set_prev(self, prev_item: 'Item') -> None:
        self._prev = prev_item  # pylint: disable=attribute-defined-outside-init  # noqa

    def set_value(self, value: int) -> None:
        self._value = value  # pylint: disable=attribute-defined-outside-init


class CircularList:
    def __init__(self, values: Tuple[int, ...]) -> None:
        self._size = len(values)
        seq = []
        for value in values:
            item = Item()
            item.set_value(value)
            seq.append(item)
        self._current = seq[0]
        seq[0].set_prev(seq[-1])
        seq[0].set_next(seq[1])

        for i in range(1, self._size - 1):
            seq[i].set_prev(seq[i - 1])
            seq[i].set_next(seq[i + 1])

        seq[-1].set_prev(seq[-2])
        seq[-1].set_next(seq[0])

        seq2 = [seq[0]] * self._size
        for item in seq:
            seq2[item.get_value() - 1] = item

        self._tuple = tuple(seq2)

    def __str__(self) -> str:
        seq = []
        item = self._current
        for _ in range(min(30, self._size)):
            seq.append(('({})' if item == self._current
                        else '{}').format(str(item.get_value())))

            # assert item.get_next().get_prev() == item

            item = item.get_next()

        # assert item == self._current

        return ' '.join(seq)

    def forward(self) -> None:
        self._current = self._current.get_next()

    def get_pickup(self) -> List[int]:
        seq = []
        item = self._current
        for _ in range(3):
            item = item.get_next()
            seq.append(item.get_value())

        return seq

    def label1(self) -> int:
        item = self._tuple[1 - 1]
        seq = []
        for _ in range(self._size - 1):
            item = item.get_next()
            seq.append(item.get_value())

        return int(''.join(map(str, seq)))

    def label2(self) -> int:
        item = self._tuple[1 - 1].get_next()

        return item.get_value() * item.get_next().get_value()

    def move_pickup(self, destination_value: int) -> None:
        pickup_prev = self._current
        pickup = pickup_prev.get_next()  # first item of pickup part

        destination_prev = pickup.get_next().get_next()
        destination = destination_prev.get_next()  # first item of destination part  # noqa

        destination_item = self._tuple[destination_value - 1]

        after_prev = destination_item
        after = after_prev.get_next()  # first item of after destination part

        pickup.set_prev(after_prev)
        destination_prev.set_next(after)

        destination.set_prev(pickup_prev)
        after_prev.set_next(pickup)

        after.set_prev(destination_prev)
        pickup_prev.set_next(destination)

    def get_current(self) -> int:
        return self._current.get_value()


def log(*params, sep: str = ' ', end: str = '\n', file=sys.stderr,
        flush: bool = True) -> None:
    if __debug__:
        if flush:
            if file == sys.stderr:
                sys.stdout.flush()
            elif file == sys.stdout:
                sys.stderr.flush()
        print(*params, sep=sep, end=end, file=file, flush=flush)


def solve(labels: Tuple[int, ...], nb_iteration: int) -> CircularList:
    circular = CircularList(labels)
    size = len(labels)
    for _ in range(nb_iteration):
        destination = circular.get_current() - 1
        exclude = [0] + circular.get_pickup()
        while destination in exclude:
            if destination != 0:
                destination -= 1
            else:
                destination = size

        circular.move_pickup(destination)
        circular.forward()

    return circular


def main() -> None:
    labels = tuple(map(int, input()))

    # Part 1
    circular1 = solve(labels, 100)
    print(circular1.label1())
    sys.stdout.flush()

    # Part 2
    circular2 = solve(labels + tuple(range(len(labels) + 1, 10**6 + 1)), 10**7)
    print(circular2.label2())


if __name__ == '__main__':
    main()
