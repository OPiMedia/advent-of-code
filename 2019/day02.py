#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
Advent of Code 2019 - Day 2: 1202 Program Alarm
https://adventofcode.com/2019/day/2

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 19, 2019
"""


#
# Functions
###########
def interprete(program: list):
    """Execute the Intcode program"""
    program = list(program)
    operations = (lambda: None,
                  lambda a, b: a + b,
                  lambda a, b: a * b)

    size = len(program)
    i = 0
    while True:
        assert i < size

        opcode = program[i]
        if opcode == 99:  # halt
            break
        else:             # + or *
            assert 1 <= opcode <= 2
            assert i + 4 < size

            operation = operations[opcode]
            pos_a, pos_b, pos_result = program[i + 1:i + 4]
            program[pos_result] = operation(program[pos_a], program[pos_b])
            i += 4

    return program


def part1():
    """Print solution of part 1"""
    assert interprete([1, 0, 0, 3, 99]) == [1, 0, 0, 2, 99]
    assert interprete([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]) == \
        [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]

    with open('inputs/day02_input.txt') as fin:
        program = list(map(int, fin.readline().split(',')))

    program[1] = 12
    program[2] = 2
    result = interprete(program)
    print(result[0])  # 2890696


def part2():
    """Print solution of part 2"""
    with open('inputs/day02_input.txt') as fin:
        program = list(map(int, fin.readline().split(',')))

    for noun in range(100):
        for verb in range(100):
            program[1] = noun
            program[2] = verb
            result = interprete(program)
            if result[0] == 19690720:
                print(100 * noun + verb)  # 8226


#
# Main
######
def main():
    """Main"""
    part1()
    part2()

if __name__ == '__main__':
    main()
