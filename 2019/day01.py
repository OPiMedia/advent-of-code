#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
Advent of Code 2019 - Day 1: The Tyranny of the Rocket Equation
https://adventofcode.com/2019/day/1

:license: GPLv3 --- Copyright (C) 2019 Olivier Pirson
:author: Olivier Pirson --- http://www.opimedia.be/
:version: December 17, 2019
"""


#
# Functions
###########
def cumulative_fuel(mass: int) -> int:
    """Compute cumulative fuel from mass"""
    result = 0
    add = mass
    while add > 0:
        add = fuel(add)
        if add > 0:
            result += add

    return result


def fuel(mass: int) -> int:
    """Compute fuel from mass"""
    return mass // 3 - 2


def part1():
    """Print solution of part 1"""
    total = 0
    with open('inputs/day01_input.txt') as fin:
        for line in fin:
            mass = int(line)
            total += fuel(mass)

    print(total)  # 3423279


def part2():
    """Print solution of part 2"""
    total = 0
    with open('inputs/day01_input.txt') as fin:
        for line in fin:
            mass = int(line)
            total += cumulative_fuel(mass)

    print(total)  # 5132018


#
# Main
######
def main():
    """Main"""
    assert fuel(12) == 2
    assert fuel(14) == 2
    assert fuel(1969) == 654
    assert fuel(100756) == 33583

    part1()

    assert cumulative_fuel(14) == 2
    assert cumulative_fuel(1969) == 966
    assert cumulative_fuel(100756) == 50346

    part2()

if __name__ == '__main__':
    main()
